import { Grid } from '@mui/material';

import { useRouter } from 'next/router';

import { BlogBody } from '@component/blog/blog-list/blog-body/blog-body';
import { BlogView } from '@component/blog/blog-list/blog-item/blog-view/blog-view';
import { MainWrapper } from '@component/main-wrapper/main-wrapper';
import { getQueryParam } from '@util/get-query-param';

import { mockBlogList } from '../../mocks/blog-list.mock';

const Index = () => {
  const { query } = useRouter();
  const blogId = getQueryParam(query.id);

  const blog = mockBlogList.find(item => item.id === blogId);

  if (blog === null || blog === undefined) {
    return null; //back();
  }

  return (
    <MainWrapper>
      <Grid px={2} pt={2}>
        <BlogView key={blog.id} blog={blog} />
        <BlogBody blog={blog} />
      </Grid>
    </MainWrapper>
  );
};

export default Index;
