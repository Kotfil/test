import { Grid, List, ListItemButton, ListItemSecondaryAction, ListItemText } from '@mui/material';

import { Link } from '@component/link/link';
import { MainWrapper } from '@component/main-wrapper/main-wrapper';
import { ColorEnum } from '@enum/color.enum';

import { mockBlogCategories } from '../../../mocks/blog-categories.mock';

interface BlogCategoriesInterface {
  text: string;
  link: string;
  count: string;
}

export const BlogCategory = () => {
  // TODO: надо куда то деть раздел темы блогов ( с низу дизайна )
  return (
    <MainWrapper hasFooter={false}>
      <Grid container justifyContent="center" pt={2}>
        <List sx={{ display: 'flex', justifyContent: 'center', flexDirection: 'column', width: '100%', maxWidth: 320 }}>
          {mockBlogCategories.map(({ text, link, count }: BlogCategoriesInterface, index: number) => (
            <Link href={`category/${link}`}>
              <ListItemButton key={index.toString()}>
                <ListItemText primary={text} />
                <ListItemSecondaryAction
                  sx={{
                    color: ColorEnum.Black,
                  }}
                >
                  {count}
                </ListItemSecondaryAction>
              </ListItemButton>
            </Link>
          ))}
        </List>
      </Grid>
    </MainWrapper>
  );
};

export default BlogCategory;
