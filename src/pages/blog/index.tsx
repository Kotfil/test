import { Grid } from '@mui/material';

import Blog from '@component/blog/blog';
import { MainWrapper } from '@component/main-wrapper/main-wrapper';

const BlogViewPage = () => (
  <MainWrapper>
    <Grid px={2} py={2}>
      <Blog />
    </Grid>
  </MainWrapper>
);

export default BlogViewPage;
