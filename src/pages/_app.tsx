import { CacheProvider, EmotionCache } from '@emotion/react';
import CssBaseline from '@mui/material/CssBaseline';
import { ThemeProvider } from '@mui/material/styles';
import { createGlobalStyle } from 'styled-components';

import Head from 'next/head';

import createEmotionCache from '../styles/createEmotionCache';
import theme from '../styles/theme';
import '../styles/calendar/calendar.css';

// Client-side cache, shared for the whole session of the user in the browser.
const clientSideEmotionCache = createEmotionCache();

const GlobalStyle = createGlobalStyle`
  html,
  body,
  #__next {
    height: 100vh;
  }
`;

function MyApp({ Component, emotionCache = clientSideEmotionCache, pageProps }: { emotionCache?: EmotionCache } & any) {
  return (
    <CacheProvider value={emotionCache}>
      <Head>
        <title>BestTera</title>
        <meta name="viewport" content="initial-scale=1, width=device-width" />
        <link rel="icon" href="/favicon.ico" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin="anonymous" />
        <link
          href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600&family=Montserrat:wght@600&family=Nunito:wght@600;700&family=Roboto&family=Syncopate:wght@400;700&display=swap"
          rel="stylesheet"
        />
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
        <GlobalStyle />
      </Head>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Component {...pageProps} />
      </ThemeProvider>
    </CacheProvider>
  );
}

export default MyApp;
