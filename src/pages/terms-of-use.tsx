import { Grid } from '@mui/material';
import { Box } from '@mui/system';
import dayjs from 'dayjs';

import { MainWrapper } from '@component/main-wrapper/main-wrapper';
import { Typography } from '@component/typography/typography';
import { UpdateTime } from '@page-style/terms-of-use/terms-of-use.style';

import { mockTermsOfUse, mockTermsOfUseHeader } from '../mocks/terms-of-use.mock';
import 'dayjs/locale/ru';

interface TermsOfUseHeaderInterface {
  greeting: string;
  description: string;
}

interface TermsOfUseInterface {
  title: string;
  text: string;
}

const TermsOfUse = () => {
  //TODO: line break into map()
  return (
    <MainWrapper hasFooter={false}>
      <Grid container py={2} justifyContent="center">
        <Typography type="interMedium20">Условия использования</Typography>
      </Grid>
      <Box px={2}>
        <UpdateTime type="interRegular12">{`Обновлено: ${dayjs().locale('ru').format('DD MMMM YYYY')}`}</UpdateTime>
      </Box>
      <Grid item py={2} px={2}>
        {mockTermsOfUseHeader.map(({ greeting, description }: TermsOfUseHeaderInterface) => (
          <>
            <Typography type="interRegular16">{greeting}</Typography>
            <Grid container justifyContent="center" py={2}>
              <Typography type="interMedium16">Общая информация</Typography>
            </Grid>
            <Typography type="interRegular16">{description}</Typography>
          </>
        ))}
        {mockTermsOfUse.map(({ title, text }: TermsOfUseInterface, index: number) => (
          <Box key={index.toString()}>
            <Box py={2}>
              <Typography type="interMedium16">{`${index + 1}. ${title}`}</Typography>
            </Box>
            <Typography type="interRegular16">{text && text.length > 0 ? text : <br />}</Typography>
          </Box>
        ))}
      </Grid>
    </MainWrapper>
  );
};

export default TermsOfUse;
