import { Grid } from '@mui/material';
import { Box } from '@mui/system';

import { MainWrapper } from '@component/main-wrapper/main-wrapper';
import { Divider } from '@component/text-divider/text-divider.styles';
import { Typography } from '@component/typography/typography';

import { mockReference } from '../mocks/reference.mock';
//TODO remove last divider
const Reference = () => {
  return (
    <MainWrapper hasFooter={false}>
      <Grid container pt={2} justifyContent="center">
        <Typography type="interMedium20">Справка</Typography>
      </Grid>
      {mockReference.map(({ question, answer }, index: number) => (
        <Box pt={2} key={index.toString()}>
          <Grid px={2} pb={2} container direction="column" justifyContent="center">
            <Typography type="interRegular14">{question}</Typography>
          </Grid>
          <Grid px={2} pb={2} container direction="column" justifyContent="center" sx={{ fontStyle: 'italic' }}>
            <Typography type="interRegular14">{answer}</Typography>
          </Grid>
          <Divider />
        </Box>
      ))}
    </MainWrapper>
  );
};
export default Reference;
