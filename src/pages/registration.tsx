import { useState } from 'react';

import { RegistrationFirst } from '@component/pages/registration/registration-first';
import { RegistrationSecond } from '@component/pages/registration/registration-second';
import { RegistrationSuccess } from '@component/pages/registration/registration-success';

const Registration = () => {
  const [registrationData, setRegistrationData] = useState<any>();
  const [secondData, setSecondData] = useState();

  const shouldRenderRegistrationFirst = registrationData === undefined;

  if (secondData !== undefined && registrationData !== undefined) {
    return <RegistrationSuccess email={registrationData.email ?? ''} />;
  }

  return shouldRenderRegistrationFirst ? (
    <RegistrationFirst onSubmit={setRegistrationData} />
  ) : (
    <RegistrationSecond onSubmit={setSecondData} />
  );
};

export default Registration;
