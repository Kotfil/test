import { Box, Grid } from '@mui/material';

import { useRouter } from 'next/router';

import { CategoriesBusinessAndHobiesOptions } from '@component/categories/categories-view/caterogies-view.options';
import { Icon } from '@component/icon/icon';
import { Link } from '@component/link/link';
import { MainWrapper } from '@component/main-wrapper/main-wrapper';
import { MentorsList } from '@component/mentors/mentors-list/mentors-list';
import { MentorsSearch } from '@component/mentors/mentors-search/mentors-search';
import { Pagination } from '@component/pagination/pagination';
import { Typography } from '@component/typography/typography';
import { CategoryFilter } from '@page-style/category/category.styles';

const ProfessionPage = () => {
  const { query } = useRouter();

  const { name } = CategoriesBusinessAndHobiesOptions.find(item => item.id === query.id) ?? {};

  return (
    <MainWrapper>
      <Grid container py={2} px={1.25}>
        <Grid item xs={12}>
          <Link href="/">
            <Icon type="Back" />
          </Link>
        </Grid>
      </Grid>
      <Grid container px={1.25}>
        <Grid item xs={12}>
          <MentorsSearch />
        </Grid>
        <Grid container py={2}>
          <CategoryFilter item>
            <Typography type="interRegular14">{name ?? ''}</Typography>
          </CategoryFilter>
        </Grid>
      </Grid>
      <Box mt={-1.5} px={1.25}>
        <MentorsList />
      </Box>
      <Grid container justifyContent="center" pt={2}>
        <Pagination count={6} siblingCount={0} />
      </Grid>
    </MainWrapper>
  );
};

export default ProfessionPage;
