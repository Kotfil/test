import { ContentWrapper, NavigationWrapper, Wrapper } from '@component/main-wrapper/main-wrapper.style';
import { MentorView } from '@component/mentor/mentor-view/mentor-view';
import { Navigation } from '@component/navigation/navigation';

import { mockMentorsList } from '../mocks/mentors-list.mock';

const Profile = () => (
  <Wrapper>
    <NavigationWrapper>
      <Navigation />
    </NavigationWrapper>
    <ContentWrapper>
      {mockMentorsList.map(mentor => (
        <MentorView key={mentor.name} mentor={mentor} />
      ))}
    </ContentWrapper>
  </Wrapper>
);

export default Profile;
