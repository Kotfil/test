import { useState } from 'react';

import { useRouter } from 'next/router';

import { ButtonList } from '@component/button/button-list/button-list';
import { MainWrapper } from '@component/main-wrapper/main-wrapper';
import { NeedAccount } from '@component/mentor/common/need-account/need-account';
import { MentorFeedbacks } from '@component/mentor/mentor-feedbacks/mentor-feedbacks';
import { MentorInfo } from '@component/mentor/mentor-info/mentor-info';
import { MentorView } from '@component/mentor/mentor-view/mentor-view';
import { MentorViewButtonWrapper, MentorViewWrapper } from '@page-style/mentor/mentor.style';
import { getQueryParam } from '@util/get-query-param';

import { mentorBlocksMock, MentorButtonEnum } from '../../mocks/mentor-blocks.mock';
import { mockMentorsList } from '../../mocks/mentors-list.mock';

const MentorViewPage = () => {
  const { query } = useRouter();
  const mentorId = getQueryParam(query.id);

  const mentor = mockMentorsList.find(item => item.id === mentorId);
  const [activeType, setActiveType] = useState(MentorButtonEnum.About);

  const handleType = (type: MentorButtonEnum) => () => setActiveType(type);

  if (mentor === null || mentor === undefined) {
    return null; //back();
  }

  return (
    <MainWrapper>
      <MentorViewWrapper>
        <MentorView mentor={mentor} />
      </MentorViewWrapper>
      <MentorViewButtonWrapper>
        <ButtonList options={mentorBlocksMock} activeType={activeType} onClick={handleType} />
      </MentorViewButtonWrapper>
      {activeType === MentorButtonEnum.About && <MentorInfo mentor={mentor} />}
      {activeType === MentorButtonEnum.Feedback && <MentorFeedbacks />}
      <NeedAccount />
    </MainWrapper>
  );
};

export default MentorViewPage;
