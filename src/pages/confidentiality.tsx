import { Grid } from '@mui/material';
import { Box } from '@mui/system';
import dayjs from 'dayjs';

import { MainWrapper } from '@component/main-wrapper/main-wrapper';
import { Typography } from '@component/typography/typography';
import { UpdateTime } from '@page-style/confidentiality/confidentiality.style';

import 'dayjs/locale/ru';
import { mockConfidentialityBody, mockConfidentialityHeader } from '../mocks/confidentiality.mock';

interface ConfidentialityHeaderInterface {
  title: string;
  description: string;
}

interface ConfidentialityBodyInterface {
  title: string;
  text: string;
}

const Confidentiality = () => {
  return (
    <MainWrapper hasFooter={false}>
      <Grid container p={2} justifyContent="center">
        <Typography type="interMedium20">Политика конфиденциальности</Typography>
      </Grid>
      <Box px={2}>
        <UpdateTime type="interRegular12">{`Обновлено: ${dayjs().locale('ru').format('DD MMMM YYYY')}`}</UpdateTime>
      </Box>
      <Grid item p={2}>
        {mockConfidentialityHeader.map(({ title, description }: ConfidentialityHeaderInterface, index: number) => (
          <Box key={index.toString()}>
            <Grid container justifyContent="center" pb={2}>
              <Typography type="interMedium16">{title}</Typography>
            </Grid>
            <Typography type="interRegular16">{description}</Typography>
          </Box>
        ))}
        {mockConfidentialityBody.map(({ title, text }: ConfidentialityBodyInterface, index: number) => (
          <Box key={index.toString()}>
            <Box py={2} pl={1}>
              <Typography type="interMedium16">{`${index + 1}. ${title}`}</Typography>
            </Box>
            <Typography type="interRegular16">{text && text.length > 0 ? text : <br />}</Typography>
          </Box>
        ))}
      </Grid>
    </MainWrapper>
  );
};

export default Confidentiality;
