import { Grid } from '@mui/material';

import NextLink from 'next/link';

import { SecondaryButton } from '@component/button/button';
import { LoginForm } from '@component/form/login-form/login-form';
import { MainWrapper } from '@component/main-wrapper/main-wrapper';
import { SocialButtons } from '@component/social/socialIcons';
import { TextDivider } from '@component/text-divider/text-divider';
import { Title } from '@page-style/login/login.style';

const Login = () => (
  <MainWrapper hasFooter={false}>
    <Grid container px={2} pt={2}>
      <Grid item xs={12} pt={0.5} justifyContent="center">
        <Title type="interMedium20">Вход</Title>
      </Grid>
      <Grid item xs={12}>
        <LoginForm />
      </Grid>
      <Grid item xs={12} pt={2}>
        <TextDivider>или</TextDivider>
      </Grid>
      <SocialButtons />
      <Grid item xs={12} pt={4}>
        <TextDivider>Нет аккаунта?</TextDivider>
      </Grid>
      <Grid item xs={12} pt={2}>
        <NextLink href="/registration">
          <SecondaryButton>Зарегистрироваться</SecondaryButton>
        </NextLink>
      </Grid>
    </Grid>
  </MainWrapper>
);

export default Login;
