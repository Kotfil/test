import { Grid } from '@mui/material';
import { Box } from '@mui/system';

import { MainWrapper } from '@component/main-wrapper/main-wrapper';
import { Typography } from '@component/typography/typography';

import { mockRefoundPolicy } from '../mocks/refound-policy.mock';

interface RefoundPolicyInterface {
  title: string;
  description: string;
}

const RefoundPolicy = () => {
  return (
    <MainWrapper hasFooter={false}>
      <Grid container justifyContent="center" py={2}>
        <Typography type="interMedium20">Политика возврата средств</Typography>
      </Grid>
      {mockRefoundPolicy.map(({ title, description }: RefoundPolicyInterface, index: number) => (
        <Box px={2} key={index.toString()}>
          <Box py={2} pl={1}>
            <Typography type="interMedium16">{`${index + 1}. ${title}`}</Typography>
          </Box>
          <Typography type="interRegular14">{description}</Typography>
        </Box>
      ))}
    </MainWrapper>
  );
};

export default RefoundPolicy;
