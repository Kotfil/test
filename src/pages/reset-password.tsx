import { useState } from 'react';

import { ResetPasswordFirst } from '@component/pages/reset-password/reset-password-first';
import { ResetPasswordSuccess } from '@component/pages/reset-password/reset-password-success';

const ResetPassword = () => {
  const [resetData, setResetData] = useState<any>();

  const handleBack = () => setResetData(undefined);

  return resetData === undefined ? (
    <ResetPasswordFirst onSubmit={setResetData} />
  ) : (
    <ResetPasswordSuccess email={resetData.email} onBack={handleBack} />
  );
};

export default ResetPassword;
