import { useState } from 'react';

import { Onboarding } from '@component/pages/onboarding/onboarding';
import { OnboardingWelcome } from '@component/pages/onboarding/onboarding-welcome';

const OnboardingMainPage = () => {
  const [isShowedOnboarding, setShowedOnboarding] = useState(false);

  const handleNext = () => setShowedOnboarding(true);

  return isShowedOnboarding ? <Onboarding /> : <OnboardingWelcome onClick={handleNext} />;
};

export default OnboardingMainPage;
