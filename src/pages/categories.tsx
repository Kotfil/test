import { CaterogiesView } from '@component/categories/categories-view/caterogies-view';
import { Separator } from '@component/categories/categories-view/caterogies-view.style';
import { ContentWrapper, NavigationWrapper, Wrapper } from '@component/main-wrapper/main-wrapper.style';
import { Navigation } from '@component/navigation/navigation';

const Categories = () => {
  return (
    <Wrapper>
      <NavigationWrapper>
        <Navigation />
      </NavigationWrapper>
      <Separator />
      <ContentWrapper>
        <CaterogiesView />
      </ContentWrapper>
    </Wrapper>
  );
};
export default Categories;
