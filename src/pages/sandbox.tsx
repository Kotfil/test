import { Box, Grid } from '@mui/material';

import { PrimaryButton, SecondaryButton, TextButton } from '@component/button/button';

export const Sandbox = () => {
  return (
    <Box>
      <Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
        <Grid item xs={6}>
          <PrimaryButton>Next</PrimaryButton>
        </Grid>
        <Grid item xs={6}>
          <SecondaryButton>Next</SecondaryButton>
        </Grid>
        <Grid item xs={6}>
          <PrimaryButton disabled>Next</PrimaryButton>
        </Grid>
        <Grid item xs={6}>
          <SecondaryButton disabled>Next</SecondaryButton>
        </Grid>
        <Grid item xs={6}>
          <TextButton>Next</TextButton>
        </Grid>
        <Grid item xs={6}>
          <TextButton disabled>Next</TextButton>
        </Grid>
      </Grid>
    </Box>
  );
};

export default Sandbox;
