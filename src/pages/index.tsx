import Pic from '@assets/home.png';
import { Box } from '@mui/material';
import React from 'react';

import type { NextPage } from 'next';
import Image from 'next/image';

import { MainMentors } from '@component/main-mentors/main-mentors';
import { MainWrapper } from '@component/main-wrapper/main-wrapper';
import { Description, ImageWrapper, Title, TitleWrapper } from '@page-style/home/home.style';

const Home: NextPage = () => (
  <MainWrapper>
    <ImageWrapper>
      <Image src={Pic} alt="" width={320} height={249} layout="responsive" objectFit="cover" placeholder="blur" />
    </ImageWrapper>
    <TitleWrapper mb={-7}>
      <Box px={2} pb={1}>
        <Title type="nunitoBold18">Платформа для консалтинга, обучения и деловых знакомств</Title>
      </Box>
      <Box px={2}>
        <Description type="interRegular12">
          Место, где можно выбрать опытного профессионала и получить консультацию для своего дела или хобби
        </Description>
      </Box>
    </TitleWrapper>
    <MainMentors />
  </MainWrapper>
);

export default Home;
