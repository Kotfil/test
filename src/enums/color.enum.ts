export enum ColorEnum {
  PrimaryColor = '#1565C0',
  DarkAccent = '#544FB0',
  Hover = '#488D5C',
  LightGrey = '#E0E0E0',
  LightGrey2 = '#C7C7C7',

  White = '#ffffff',
  SuperLightGrey = '#F1F1F1',
  MiddleGrey = '#A0A0A0',
  MainText = '#1F1F1F',
  Black = '#000000',
  Blue = '#2F80ED',
  Dark = '#3A3D40',
  Grey = '#666666',
  Grey2 = '#4F4F4F',

  Aluminium = '#A5A7AC',
  Mandy = '#E65666',
}
