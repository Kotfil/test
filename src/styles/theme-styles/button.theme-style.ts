import { ColorEnum } from '@enum/color.enum';

export const ButtonThemeStyle = {
  default: {
    padding: '8px 0px',
    width: '100%',
    textTransform: 'none',
  },
  defaultBorder: {
    border: `1px solid ${ColorEnum.PrimaryColor}`,
    borderRadius: 24,
    padding: '8px 48px',
    width: '100%',
    textTransform: 'capitalize',
  },
  empty: {
    padding: 0,
    minWidth: 'auto',
  },
};
