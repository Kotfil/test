import { createTheme } from '@mui/material/styles';
import { ButtonMuiTheme } from '@style/mui-theme/button.mui-theme';

import { ColorEnum } from '@enum/color.enum';

// Create a theme instance.
const theme = createTheme({
  palette: {
    primary: {
      main: ColorEnum.PrimaryColor,
      dark: ColorEnum.PrimaryColor,
    },
    secondary: {
      main: 'rgba(0,0,0,0)',
    },
    action: {
      active: ColorEnum.DarkAccent,
    },
  },
  components: {
    MuiButton: {
      variants: ButtonMuiTheme,
    },
  },
});

export default theme;

declare module '@mui/material/Button' {
  interface ButtonPropsVariantOverrides {
    transparent: true;
    empty: true;
  }
}
