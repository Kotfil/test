import { ComponentsVariants } from '@mui/material/styles/variants';
import { Interpolation, Theme } from '@mui/system';
import { ButtonThemeStyle } from '@style/theme-styles/button.theme-style';
import { FontThemeStyle } from '@style/theme-styles/font.theme-style';

import { ColorEnum } from '@enum/color.enum';

export const ButtonMuiTheme: ComponentsVariants['MuiButton'] = [
  {
    props: { variant: 'text', color: 'primary' },
    style: {
      ...FontThemeStyle.interRegular16,
      ...ButtonThemeStyle.default,
    } as Interpolation<{ theme: Theme }>,
  },
  {
    props: { variant: 'text', color: 'secondary' },
    style: {
      ...FontThemeStyle.interRegular16,
      ...ButtonThemeStyle.default,
      color: ColorEnum.LightGrey2,
    } as Interpolation<{ theme: Theme }>,
  },
  {
    props: { variant: 'contained', color: 'primary' },
    style: {
      ...FontThemeStyle.interRegular16,
      ...ButtonThemeStyle.defaultBorder,
      ':disabled': {
        borderColor: ColorEnum.LightGrey,
        backgroundColor: ColorEnum.LightGrey,
      },
    } as Interpolation<{ theme: Theme }>,
  },
  {
    props: { variant: 'transparent', color: 'secondary' },
    style: {
      color: ColorEnum.PrimaryColor,
      ...FontThemeStyle.interRegular16,
      ...ButtonThemeStyle.defaultBorder,
      ':disabled': {
        borderColor: ColorEnum.LightGrey,
      },
    } as Interpolation<{ theme: Theme }>,
  },
  {
    props: { variant: 'empty' },
    style: {
      ...FontThemeStyle.interRegular16,
      ...ButtonThemeStyle.empty,
    } as Interpolation<{ theme: Theme }>,
  },
];
