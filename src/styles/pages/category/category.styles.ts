import { Grid } from '@mui/material';
import styled from 'styled-components';

import { ColorEnum } from '@enum/color.enum';

export const CategoryFilter = styled(Grid)`
  padding: 4px 8px;
  color: ${ColorEnum.White};
  background-color: ${ColorEnum.PrimaryColor};
  border-radius: 6px;
`;
