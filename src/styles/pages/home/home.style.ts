import { Box } from '@mui/material';
import styled from 'styled-components';

import { Typography } from '@component/typography/typography';
import { ColorEnum } from '@enum/color.enum';

export const Title = styled(Typography)`
  color: ${ColorEnum.MainText};
`;

export const Description = styled(Typography)`
  color: ${ColorEnum.Grey2};
`;

export const ImageWrapper = styled(Box)`
  overflow: hidden;
  position: relative;
  width: 100%;
  height: 249px;
`;

export const TitleWrapper = styled(Box)`
  transform: translateY(-54px);
`;
