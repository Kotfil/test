import styled from 'styled-components';

import { Typography } from '@component/typography/typography';
import { ColorEnum } from '@enum/color.enum';

export const UpdateTime = styled(Typography)`
  color: ${ColorEnum.Grey};
`;
