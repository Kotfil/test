import styled from 'styled-components';

export const MentorViewWrapper = styled.div`
  padding: 8px 16px 0;
`;

export const MentorViewButtonWrapper = styled.div`
  padding: 24px 10px 0;
`;
