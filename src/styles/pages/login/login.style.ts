import styled from 'styled-components';

import { Input } from '@component/input/input';
import { Typography } from '@component/typography/typography';
import { ColorEnum } from '@enum/color.enum';

export const InputForm = styled(Input)`
  padding: 0 16px;
  color: ${ColorEnum.Grey};
  font-size: 16px;
  width: 100%;
`;

export const Title = styled(Typography)`
  display: block;
  text-align: center;
`;

export const LostPassword = styled(Typography)`
  color: ${ColorEnum.Blue};
  display: block;
  text-align: center;
`;
