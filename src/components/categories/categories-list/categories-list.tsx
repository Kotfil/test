import { Grid } from '@mui/material';

import { CategoriesListItemInterface } from './category-list-item/categories-list-item.interface';
import { CategoryListItem } from './category-list-item/category-list-item';

interface CategoriesListProps {
  list: CategoriesListItemInterface[];
}

export const CategoriesList = ({ list }: CategoriesListProps) => (
  <Grid container rowSpacing={{ xs: 2, sm: 2, md: 3 }} columnSpacing={{ xs: 1, sm: 2, md: 2 }}>
    {list.map(item => (
      <CategoryListItem key={item.id} id={item.id} image={item.image} name={item.name} />
    ))}
  </Grid>
);
