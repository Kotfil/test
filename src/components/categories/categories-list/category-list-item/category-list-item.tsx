import { Grid } from '@mui/material';

import NextLink from 'next/link';

import { CategoriesListItemInterface } from './categories-list-item.interface';
import {
  CategoryListItemImage,
  CategoryListItemImageWrapper,
  CategoryListItemLink,
  CategoryListItemWrapper,
  Title,
} from './category-list-item.styles';

export const CategoryListItem = ({ id, image, name }: CategoriesListItemInterface) => (
  <Grid item xs={6} sm={4} md={3}>
    <CategoryListItemWrapper>
      <NextLink href={`/profession/${id}`}>
        <CategoryListItemLink>
          <CategoryListItemImageWrapper>
            <CategoryListItemImage src={image} />
          </CategoryListItemImageWrapper>
          <Title>{name}</Title>
        </CategoryListItemLink>
      </NextLink>
    </CategoryListItemWrapper>
  </Grid>
);
