import styled from 'styled-components';

import { ColorEnum } from '@enum/color.enum';

const timeAnimation = 1 / 4;

export const CategoryListItemWrapper = styled.li`
  position: relative;
  height: 100px;
  overflow: hidden;
  list-style: none;
  border-radius: 6px;
`;

export const CategoryListItemImage = styled.img`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const CategoryListItemImageWrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;

  ::after {
    position: absolute;
    top: 0;
    left: 0;
    display: block;
    width: 100%;
    height: 100%;
    background-color: #0000003f;
    transition: background-color ease-in ${timeAnimation}s;
    content: '';
  }
`;

export const CategoryListItemLink = styled.a`
  :hover {
    ${CategoryListItemImageWrapper} {
      &::after {
        background-color: #0000007f;
      }
    }
  }
`;

export const Title = styled.p`
  position: absolute;
  bottom: 0;
  left: 0;
  color: ${ColorEnum.White};
  font-weight: 500;
  font-size: 13px;
  font-family: Roboto, sans-serif;
  line-height: 15px;
  transform: translateX(8px) translateY(-8px);
`;
