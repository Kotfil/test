export interface CategoriesListItemInterface {
  id: string;
  name: string;
  image: string;
}
