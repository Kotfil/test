import styled from 'styled-components';

export const CategoriesListWrapper = styled.ul`
  display: grid;
  grid-template-columns: 1fr 1fr;
  gap: 8px 16px;
  padding: 24px 0;
`;
