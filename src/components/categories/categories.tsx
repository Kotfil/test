import { Grid } from '@mui/material';
import React, { useCallback, useRef, useState } from 'react';
import { useHotkeys } from 'react-hotkeys-hook';
import { useSwipeable } from 'react-swipeable';

import { PrimaryButton } from '@component/button/button';

import { CategoriesList } from './categories-list/categories-list';
import { CategoriesBusinessOptions, CategoriesHobieOptions } from './categories.options';
import { ModalCategoryWrapper, Title } from './categories.styles';

interface CategoriesProps {
  type: string;
}

const DEFAULT_TYPE = 'business';

export const Categories = ({ type }: CategoriesProps) => {
  const scrollPosition = useRef(0);
  const [isOpened, setOpen] = useState(false);

  useHotkeys('escape', () => setOpen(false));
  const handleOpen = useCallback(() => setOpen(true), []);
  const handlers = useSwipeable({
    preventDefaultTouchmoveEvent: true,
    trackTouch: true,
    trackMouse: true,
    onSwipeStart: ({ event }) => {
      event.preventDefault();
      scrollPosition.current = (event.currentTarget as HTMLDivElement)?.scrollTop;
    },
    onSwipedDown: ({ event }) => {
      if ((event.currentTarget as HTMLDivElement)?.scrollTop === 0 && scrollPosition.current === 0) {
        event.stopPropagation();
        setOpen(false);
      }
    },
  });

  return (
    <>
      <Grid pt={5}>
        <CategoriesList list={type === DEFAULT_TYPE ? CategoriesBusinessOptions : CategoriesHobieOptions} />
        <Grid item pt={5}>
          <PrimaryButton onClick={handleOpen}>Посмотреть все</PrimaryButton>
        </Grid>
      </Grid>
      <ModalCategoryWrapper isOpened={isOpened} {...handlers}>
        <Grid px={2}>
          <Grid item pb={2}>
            <Title type="interMedium20" tag="h1">
              Выберите профессионала
            </Title>
          </Grid>
          <Grid item p={0}>
            <CategoriesList list={type === DEFAULT_TYPE ? CategoriesBusinessOptions : CategoriesHobieOptions} />
          </Grid>
        </Grid>
      </ModalCategoryWrapper>
    </>
  );
};
