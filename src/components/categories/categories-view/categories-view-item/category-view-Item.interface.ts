export interface CategoriesViewItemProps {
  id: string;
  name: string;
  image: string;
}
