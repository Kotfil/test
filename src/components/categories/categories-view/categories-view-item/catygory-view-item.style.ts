import styled from 'styled-components';

import { ColorEnum } from '@enum/color.enum';

const timeAnimation = 1 / 4;

export const CategoryViewItemWrapper = styled.li`
  position: relative;
  height: 100px;
  overflow: hidden;
  border-radius: 6px;
`;

export const CategoryViewItemImage = styled.img`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const CategoryViewItemImageWrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;

  ::after {
    position: absolute;
    top: 0;
    left: 0;
    display: block;
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.25);
    transition: background-color ease-in ${timeAnimation}s;
    content: '';
  }
`;

export const CategoryViewItemLink = styled.a`
  :hover {
    ${CategoryViewItemImageWrapper} {
      &::after {
        background-color: rgba(0, 0, 0, 0.5);
      }
    }
  }
`;

export const Title = styled.p`
  position: absolute;
  bottom: 0;
  left: 0;
  color: ${ColorEnum.White};
  font-weight: 500;
  font-size: 13px;
  font-family: 'Roboto', sans-serif;
  line-height: 15px;
  transform: translateX(8px) translateY(-8px);
`;
