import NextLink from 'next/link';

import { CategoriesViewItemProps } from './category-view-Item.interface';
import {
  CategoryViewItemLink,
  CategoryViewItemWrapper,
  CategoryViewItemImageWrapper,
  CategoryViewItemImage,
  Title,
} from './catygory-view-item.style';

export const CategoryViewItem = ({ id, image, name }: CategoriesViewItemProps) => (
  <CategoryViewItemWrapper>
    <NextLink href={`/profession/${id}`}>
      <CategoryViewItemLink>
        <CategoryViewItemImageWrapper>
          <CategoryViewItemImage src={image} />
        </CategoryViewItemImageWrapper>
        <Title>{name}</Title>
      </CategoryViewItemLink>
    </NextLink>
  </CategoryViewItemWrapper>
);
