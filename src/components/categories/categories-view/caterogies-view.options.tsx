export const CategoriesBusinessAndHobiesOptions = [
  {
    id: '1',
    name: 'Юридический',
    image: 'https://via.placeholder.com/138x100',
  },
  {
    id: '2',
    name: 'Финансовый',
    image: 'https://via.placeholder.com/438x120',
  },
  {
    id: '3',
    name: 'Обучающий',
    image: 'https://via.placeholder.com/138x100',
  },
  {
    id: '4',
    name: 'Медицинский',
    image: 'https://via.placeholder.com/138x100',
  },
  {
    id: '5',
    name: 'Hobie - Юридический',
    image: 'https://via.placeholder.com/138x100',
  },
  {
    id: '6',
    name: 'Hobie - Финансовый',
    image: 'https://via.placeholder.com/138x100',
  },
  {
    id: '7',
    name: 'Hobie - Обучающий',
    image: 'https://via.placeholder.com/138x100',
  },
  {
    id: '8',
    name: 'Hobie - Медицинский',
    image: 'https://via.placeholder.com/138x100',
  },
  {
    id: '9',
    name: 'Hobie - Обучающий',
    image: 'https://via.placeholder.com/138x100',
  },
  {
    id: '10',
    name: 'Hobie - Медицинский',
    image: 'https://via.placeholder.com/138x100',
  },
  {
    id: '11',
    name: 'Hobie - Финансовый',
    image: 'https://via.placeholder.com/138x100',
  },
  {
    id: '12',
    name: 'Hobie - Юридический',
    image: 'https://via.placeholder.com/138x100',
  },
];
