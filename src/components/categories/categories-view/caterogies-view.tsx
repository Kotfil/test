import { CategoryViewItem } from '@component/categories/categories-view/categories-view-item/category-view-Item';
import { CategoriesBusinessAndHobiesOptions } from '@component/categories/categories-view/caterogies-view.options';
import {
  CategoriesViewListWrapper,
  CategoriesViewText,
  Wrapper,
} from '@component/categories/categories-view/caterogies-view.style';

export const CaterogiesView = () => {
  return (
    <Wrapper>
      <CategoriesViewText>Выберите тему</CategoriesViewText>
      <CategoriesViewListWrapper>
        {CategoriesBusinessAndHobiesOptions.map(item => (
          <CategoryViewItem key={item.id} id={item.id} image={item.image} name={item.name} />
        ))}
      </CategoriesViewListWrapper>
    </Wrapper>
  );
};
