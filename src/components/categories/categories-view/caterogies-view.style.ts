import styled from 'styled-components';

import { ColorEnum } from '@enum/color.enum';

export const Wrapper = styled.section`
  padding: 16px;
`;

export const Separator = styled.div`
  height: 0;
  padding: 8px 0;
  border-bottom: 1px solid ${ColorEnum.LightGrey};
`;

export const CategoriesViewText = styled.div`
  color: ${ColorEnum.Black};
  font-weight: 500;
  font-size: 16px;
  font-family: 'Inter', sans-serif;
  font-style: normal;
  line-height: 19px;
  text-align: start;
`;
export const CategoriesViewListWrapper = styled.ul`
  display: grid;
  grid-template-columns: 1fr 1fr;
  gap: 8px 16px;
  padding: 24px 0;
`;
