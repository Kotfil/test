import styled from 'styled-components';

import { Typography } from '@component/typography/typography';
import { ColorEnum } from '@enum/color.enum';

interface ModalCategoryWrapperProps {
  isOpened: boolean;
}

export const ModalCategoryWrapper = styled.div<ModalCategoryWrapperProps>`
  position: fixed;
  top: 60px;
  right: 0;
  bottom: 0;
  left: 0;
  padding-bottom: 24px;
  overflow-y: auto;
  background-color: ${ColorEnum.White};
  transform: translateY(${({ isOpened }) => (isOpened ? 0 : '100%')});
  transition: transform ${1 / 2}s cubic-bezier(0.33, 1, 0.68, 1);
`;

export const Title = styled(Typography)`
  color: ${ColorEnum.MainText};
`;
