import { MentorSector } from '@component/mentor/common/sector/mentor-sector';
import { MentorTag } from '@component/mentor/mentor-view/mentor-view.interface';
import { Typography } from '@component/typography/typography';

interface MentorLanguageProps {
  language: MentorTag[];
}

export const MentorLanguage = ({ language }: MentorLanguageProps) => (
  <MentorSector title="Разговорные языки" icon="LangInfo">
    <Typography type="interRegular16">{language.map(({ name }) => name).join(', ')}</Typography>
  </MentorSector>
);
