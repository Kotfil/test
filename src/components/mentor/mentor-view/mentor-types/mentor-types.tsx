import { TextButton } from '@component/button/button';

import { ButtonItemWrapper, ButtonWrapper, Wrapper } from './mentor-types.style';

export const MentorTypes = () => {
  return (
    <Wrapper>
      <ButtonWrapper>
        <ButtonItemWrapper>
          <TextButton>Новости</TextButton>
        </ButtonItemWrapper>
        <ButtonItemWrapper>
          <TextButton>Менторство</TextButton>
        </ButtonItemWrapper>
      </ButtonWrapper>
    </Wrapper>
  );
};
