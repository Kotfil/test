import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 16px 0;
`;

export const ButtonWrapper = styled.ul`
  display: flex;
  flex-direction: row;
`;

export const ButtonItemWrapper = styled.li`
  display: flex;
  flex: 1;
`;
