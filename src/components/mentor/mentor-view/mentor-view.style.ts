import { Rating } from '@mui/material';
import styled from 'styled-components';

import { Typography } from '@component/typography/typography';
import { ColorEnum } from '@enum/color.enum';

export const Name = styled(Typography)`
  color: ${ColorEnum.Dark};
`;

export const Category = styled(Typography)`
  color: ${ColorEnum.MainText};
`;

export const Profession = styled(Typography)`
  color: ${ColorEnum.Black};
`;

export const SubTitle = styled(Typography)`
  color: ${ColorEnum.Aluminium};
`;

export const StyledRating = styled(Rating)({
  '& .MuiRating-iconFilled': {
    color: ColorEnum.Mandy,
    fontSize: 16,
  },
  '& .MuiRating-iconEmpty': {
    fontSize: 16,
  },
  '& .MuiRating-iconHover': {
    color: ColorEnum.Mandy,
  },
});

export const MentorAvatar = styled.img`
  width: 72px;
  height: 72px;
  object-fit: cover;
  border-radius: 50%;
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding-top: 16px;
  cursor: pointer;

  :hover {
    background-color: #f6f6f6;
  }
`;
