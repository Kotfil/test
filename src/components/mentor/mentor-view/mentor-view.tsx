import { Grid } from '@mui/material';

import { Icon } from '@component/icon/icon';

import { MentorProps } from './mentor-view.interface';
import { Category, MentorAvatar, Name, Profession, StyledRating, SubTitle } from './mentor-view.style';

export const MentorView = ({
  mentor: { name, experience, avatar, star, vote, location, price, category, hasLink = false },
}: MentorProps) => (
  <Grid container spacing={0} pt={1}>
    <Grid item xs={3} pt={1}>
      <Grid container spacing={0} direction="column">
        <Grid item>
          <MentorAvatar src={avatar} />
        </Grid>
        <Grid item ml={-(3 / 8)}>
          <StyledRating size="small" name="customized-color" defaultValue={star} precision={0.5} readOnly />
        </Grid>
        <Grid item mt={-1.5}>
          <SubTitle type="interRegular10">{vote} голосов</SubTitle>
        </Grid>
      </Grid>
    </Grid>
    <Grid item xs={9} pt={1}>
      <Grid container>
        <Grid item xs={12} md={6}>
          <Grid item xs={12} pl={2}>
            <Grid container>
              <Grid item xs={hasLink ? 10 : 12}>
                <Name type="montserratSemiBold18">{name}</Name>
              </Grid>
              {hasLink && (
                <Grid item xs={2} justifyContent="center">
                  <a href="#">
                    <Icon type="Link" />
                  </a>
                </Grid>
              )}
            </Grid>
          </Grid>
          <Grid item xs={12} pl={2} mt={-0.5}>
            <Category type="interBold12">{category}</Category>
          </Grid>
          <Grid item xs={12} pl={2} mt={-0.5}>
            <Profession type="interLight12">{category}</Profession>
          </Grid>
          <Grid item xs={12} pl={2}>
            <Grid container>
              <Grid item xs={8}>
                <SubTitle type="interRegular12">Локация</SubTitle>
              </Grid>
              <Grid item xs={4}>
                <SubTitle type="interRegular12">Опыт</SubTitle>
              </Grid>
            </Grid>
            <Grid container mt={-0.5}>
              <Grid item xs={8}>
                <Category type="nunitoBold14">{location}</Category>
              </Grid>
              <Grid item xs={4}>
                <Profession type="montserratMedium10">{experience} года</Profession>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12} md={6}>
          <Grid item xs={12} pl={2}>
            <Grid container>
              <Grid item xs={8}>
                <Category type="interRegular12">Консультация</Category>
              </Grid>
              <Grid item xs={4}>
                <Category type="montserratMedium10">{price}$ / час</Category>
              </Grid>
            </Grid>
            <Grid container>
              <Grid item xs={8}>
                <Category type="interRegular12">Консультация 2</Category>
              </Grid>
              <Grid item xs={4}>
                <Category type="montserratMedium10">15$ / час</Category>
              </Grid>
            </Grid>
            <Grid container>
              <Grid item xs={8}>
                <Category type="interRegular12">Консультация 3</Category>
              </Grid>
              <Grid item xs={4}>
                <Category type="montserratMedium10">5$ / час</Category>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  </Grid>
);
