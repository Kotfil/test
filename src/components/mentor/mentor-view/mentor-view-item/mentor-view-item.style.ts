import styled from 'styled-components';

import { ColorEnum } from '@enum/color.enum';

import Location from './assets/location.svg';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding-top: 16px;
`;

export const MentorWrapper = styled.div`
  display: flex;
  padding-bottom: 16px;
  padding-left: 16px;
`;

export const MentorAvatar = styled.img`
  flex: 0 0 70px;
  width: 70px;
  height: 70px;
  object-fit: cover;
  border-radius: 50%;
`;
export const MentorInfo = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  margin-left: 16px;
`;

export const MentorName = styled.p`
  font-weight: 500;
  font-size: 16px;
  font-family: 'Inter', sans-serif;
  line-height: 19px;
`;

export const MentorLocation = styled(MentorName)`
  font-weight: 400;
  font-size: 14px;
  font-style: normal;
  line-height: 17px;

  &::after {
    background: url('${Location}');
    content: '';
  }
`;

export const RatingWrapper = styled.p`
  display: flex;
  justify-content: start;
`;

export const MentorLocationWrapper = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const IconWrapper = styled.div`
  margin-right: 16px;
`;

export const StatisticsWrapper = styled.div`
  display: grid;
  grid-template-rows: 1fr;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  gap: 0 0;
`;

export const Stats = styled.div`
  display: flex;
  justify-content: center;
  font-weight: 500;
  font-size: 16px;
  font-family: 'Inter', sans-serif;
  line-height: 19px;
`;

export const CatrgoryStats = styled(Stats)`
  color: ${ColorEnum.Grey};
  font-weight: 400;
  font-size: 14px;
  line-height: 16px;
`;
