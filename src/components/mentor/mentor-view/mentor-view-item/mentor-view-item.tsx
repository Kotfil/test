import { Box } from '@mui/material';
import { Rating } from 'react-simple-star-rating/dist';

import NextLink from 'next/link';

import { PrimaryButton } from '@component/button/button';
import { Icon } from '@component/icon/icon';
import { MentorTypes } from '@component/mentor/mentor-view/mentor-types/mentor-types';
import {
  IconWrapper,
  MentorAvatar,
  MentorInfo,
  MentorLocation,
  MentorLocationWrapper,
  MentorName,
  MentorWrapper,
  RatingWrapper,
} from '@component/mentor/mentor-view/mentor-view-item/mentor-view-item.style';
import { MentorStatistics } from '@component/mentor/mentor-view/mentor-view-item/mentor-view-statistic/mentor-view-statistic';
import { MentorsFullViewInterface } from '@component/mentor/mentor-view/mentor-view.interface';
import { Wrapper } from '@component/mentor/mentor-view/mentor-view.style';
import { ColorEnum } from '@enum/color.enum';

export const MentorViewItem = ({
  name,
  meeting,
  feedback,
  price,
  experience,
  location,
  avatar,
  star,
}: MentorsFullViewInterface) => {
  return (
    <Wrapper>
      <MentorWrapper>
        <MentorAvatar src={avatar} />
        <MentorInfo>
          <MentorName>{name}</MentorName>
          <MentorLocationWrapper>
            <MentorLocation>{location}</MentorLocation>
            <IconWrapper>
              <Icon type="Send" />
            </IconWrapper>
          </MentorLocationWrapper>
          <RatingWrapper>
            <Rating
              ratingValue={star}
              size={12}
              onClick={() => {}}
              fillColor={ColorEnum.PrimaryColor}
              emptyColor={ColorEnum.LightGrey}
            />
          </RatingWrapper>
        </MentorInfo>
      </MentorWrapper>
      <MentorStatistics experience={experience} price={price} feedback={feedback} meeting={meeting} />
      <MentorTypes />
      <Box>
        <NextLink href="/registration">
          <PrimaryButton>Зарегистрироваться</PrimaryButton>
        </NextLink>
      </Box>
    </Wrapper>
  );
};
