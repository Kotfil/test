import {
  CatrgoryStats,
  StatisticsWrapper,
  Stats,
} from '@component/mentor/mentor-view/mentor-view-item/mentor-view-item.style';

export const MentorStatistics = ({ price, experience, feedback, meeting }: any) => {
  return (
    <>
      <StatisticsWrapper>
        <Stats>{`${price}$`}</Stats>
        <Stats>{`${experience} года`}</Stats>
        <Stats>{feedback}</Stats>
        <Stats>{meeting}</Stats>
      </StatisticsWrapper>
      <StatisticsWrapper>
        <CatrgoryStats>За час</CatrgoryStats>
        <CatrgoryStats>На сервисе</CatrgoryStats>
        <CatrgoryStats>Встреч</CatrgoryStats>
        <CatrgoryStats>Отзывов</CatrgoryStats>
      </StatisticsWrapper>
    </>
  );
};