import styled from 'styled-components';

export const Statistics = styled.span`
  color: #a600ff;
`;
export const Wrapper = styled.div`
  display: flex;
  justify-content: center;
`;

export const MentorStatistics = styled.div`
  display: flex;
  justify-content: center;
`;
