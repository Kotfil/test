export interface MentorTag {
  id: string;
  name: string;
}

export interface MentorsViewInterface {
  id: string;
  name: string;
  skills: MentorTag[];
  tags: MentorTag[];
  language: MentorTag[];
  format: MentorTag[];
  services: MentorTag[];
  avatar: string;
  star: number;
  vote: number;
  location: string;
  category: string;
  link: string;
  price: number;
  experience: number;
  hasLink?: boolean;
  description: string;
}

export interface MentorsFullViewInterface {
  name: string;
  avatar: string;
  location: string;
  price: number;
  star: number;
  experience: number;
  meeting: number;
  feedback: number;
}

export interface MentorProps {
  mentor: MentorsViewInterface;
}
