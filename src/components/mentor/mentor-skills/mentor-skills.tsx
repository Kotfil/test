import { Grid } from '@mui/material';

import { IconEnum } from '@component/icon/icon.enum';
import { MentorSector } from '@component/mentor/common/sector/mentor-sector';
import { MentorTag } from '@component/mentor/mentor-view/mentor-view.interface';

import { Skill, SkillTitle } from './mentor-skills.styles';

interface MentorSkillsProps {
  title: string;
  skills: MentorTag[];
  icon?: IconEnum;
}

export const MentorSkills = ({ skills, title, icon = 'Support' }: MentorSkillsProps) => (
  <MentorSector title={title} icon={icon}>
    <Grid container direction="row" ml={-0.5} mb={-1}>
      {skills.map(({ id, name }) => (
        <Skill item key={id} px={2} py={1} mx={0.5} mb={1}>
          <SkillTitle type="interRegular14">{name}</SkillTitle>
        </Skill>
      ))}
    </Grid>
  </MentorSector>
);
