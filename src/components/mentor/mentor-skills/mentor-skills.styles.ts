import { Grid } from '@mui/material';
import styled from 'styled-components';

import { Typography } from '@component/typography/typography';
import { ColorEnum } from '@enum/color.enum';

export const Skill = styled(Grid)`
  border: 1px solid ${ColorEnum.MiddleGrey};
  border-radius: 6px;
`;

export const SkillTitle = styled(Typography)`
  color: ${ColorEnum.MiddleGrey};
`;
