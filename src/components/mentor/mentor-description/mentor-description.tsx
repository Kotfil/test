import { Box } from '@mui/material';

import { MentorSector } from '@component/mentor/common/sector/mentor-sector';
import { Typography } from '@component/typography/typography';

interface MentorDescriptionProps {
  description: string;
}

export const MentorDescription = ({ description }: MentorDescriptionProps) => (
  <MentorSector title="Описание опыта" icon="Description">
    <Box pl={0.25} pr={1}>
      <Typography type="interRegular16">{description}</Typography>
    </Box>
  </MentorSector>
);
