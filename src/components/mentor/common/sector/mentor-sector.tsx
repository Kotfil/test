import { Grid } from '@mui/material';
import { FC } from 'react';

import { Icon } from '@component/icon/icon';
import { IconEnum } from '@component/icon/icon.enum';

import { Title } from './mentor-sector.styles';

interface MentorSectorProps {
  icon: IconEnum;
  title: string;
  justify?: string;
}

export const MentorSector: FC<MentorSectorProps> = ({ title, justify, icon, children }) => (
  <Grid container>
    <Grid container>
      <Grid item>
        <Icon type={icon} />
      </Grid>
      <Grid item pl={1}>
        <Title type="nunitoBold18">{title}</Title>
      </Grid>
    </Grid>
    <Grid container justifyContent={justify} pt={2}>
      {children}
    </Grid>
  </Grid>
);
