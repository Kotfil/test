import styled from 'styled-components';

import { Typography } from '@component/typography/typography';
import { ColorEnum } from '@enum/color.enum';

export const Title = styled(Typography)`
  color: ${ColorEnum.MainText};
`;
