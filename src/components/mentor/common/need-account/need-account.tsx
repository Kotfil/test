import { Grid } from '@mui/material';

import NextLink from 'next/link';

import { SecondaryButton } from '@component/button/button';
import { TextDivider } from '@component/text-divider/text-divider';

export const NeedAccount = () => (
  <Grid container px={2}>
    <TextDivider>Нет аккаунта?</TextDivider>
    <Grid item xs={12} pt={1}>
      <NextLink href="/registration">
        <SecondaryButton>Зарегистрироваться</SecondaryButton>
      </NextLink>
    </Grid>
  </Grid>
);
