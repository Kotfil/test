import { Box, Divider, Link, List, ListItem } from '@mui/material';

import { MentorSector } from '@component/mentor/common/sector/mentor-sector';
import { MentorCalendar } from '@component/mentor/mentor-calendar/mentor-calendar';
import { MentorDescription } from '@component/mentor/mentor-description/mentor-description';
import { MentorLanguage } from '@component/mentor/mentor-language/mentor-language';
import { MentorSkills } from '@component/mentor/mentor-skills/mentor-skills';
import { MentorsViewInterface } from '@component/mentor/mentor-view/mentor-view.interface';
import { Typography } from '@component/typography/typography';
import { ColorEnum } from '@enum/color.enum';

export interface MentorProps {
  mentor: MentorsViewInterface;
}

export const MentorInfo = ({ mentor }: MentorProps) => (
  <Box px={1.25}>
    <List disablePadding>
      <ListItem disableGutters>
        <Box pb={1}>
          <MentorDescription description={mentor.description} />
        </Box>
      </ListItem>
      <Divider light />
      <ListItem disableGutters>
        <Box py={1}>
          <MentorSkills title="Основные навыки" skills={mentor.skills} />
        </Box>
      </ListItem>
      <Divider light />
      <ListItem disableGutters>
        <Box py={1}>
          <MentorSkills title="Дополнительные навыки" skills={mentor.tags} />
        </Box>
      </ListItem>
      <Divider light />
      <ListItem disableGutters>
        <Box py={1}>
          <MentorLanguage language={mentor.language} />
        </Box>
      </ListItem>
      <Divider light />
      <ListItem disableGutters>
        <Box py={1}>
          <MentorSkills title="Формат встречи" icon="Format" skills={mentor.format} />
        </Box>
      </ListItem>
      <Divider light />
      <ListItem disableGutters>
        <Box py={1}>
          <MentorSkills title="Виды услуг" icon="Services" skills={mentor.services} />
        </Box>
      </ListItem>
      <Divider light />
      <ListItem disableGutters>
        <Box py={1}>
          <MentorSector title="Ссылка на Linkedin" icon="LinkedInFill">
            <Link href={mentor.link} underline="none" rel="noreferrer" target="_blank" color={ColorEnum.MiddleGrey}>
              <Typography type="interRegular16">{mentor.link}</Typography>
            </Link>
          </MentorSector>
        </Box>
      </ListItem>
      <Divider light />
      <ListItem disableGutters>
        <MentorCalendar />
      </ListItem>
    </List>
  </Box>
);
