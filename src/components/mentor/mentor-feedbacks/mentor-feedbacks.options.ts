export enum MentorFilterEnum {
  New = 'New',
  Old = 'Old',
}

export const MentorFeedbacksOptions = [
  {
    value: MentorFilterEnum.New,
    label: 'От новых к прошлым',
  },
  {
    value: MentorFilterEnum.Old,
    label: 'От прошлых к новым',
  },
];
