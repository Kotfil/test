import { Grid, Rating } from '@mui/material';
import styled from 'styled-components';

import { Typography } from '@component/typography/typography';
import { ColorEnum } from '@enum/color.enum';

export const Name = styled(Typography)`
  color: ${ColorEnum.MainText};
`;

export const Time = styled(Typography)`
  color: ${ColorEnum.Grey};
`;

export const StyledRating = styled(Rating)({
  '& .MuiRating-iconFilled': {
    color: ColorEnum.PrimaryColor,
    fontSize: 16,
  },
  '& .MuiRating-iconEmpty': {
    fontSize: 16,
  },
  '& .MuiRating-iconHover': {
    color: ColorEnum.PrimaryColor,
  },
});

export const MentorAvatar = styled.img`
  width: 48px;
  height: 48px;
  object-fit: cover;
  border-radius: 50%;
`;

export const Wrapper = styled(Grid)`
  padding: 8px 0;
  border-bottom: 1px solid ${ColorEnum.LightGrey};
`;
