import { Box, Grid } from '@mui/material';
import dayjs from 'dayjs';

import { Typography } from '@component/typography/typography';

import { FeedbackProps } from './mentor-feedback.interface';
import { Time, MentorAvatar, Name, StyledRating, Wrapper } from './mentor-feedback.style';

export const MentorFeedback = ({ feedback: { name, avatar, star, feedback, date } }: FeedbackProps) => (
  <Wrapper container spacing={0} direction="column">
    <Grid container spacing={0} pt={1}>
      <Grid item pt={1}>
        <Grid container spacing={0} direction="column">
          <Grid item>
            <MentorAvatar src={avatar} />
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={10} pt={1}>
        <Grid container>
          <Grid item xs={12} md={6}>
            <Grid item xs={12} pl={2}>
              <Grid container>
                <Grid item xs={12}>
                  <Name type="nunitoMedium18">{name}</Name>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12} pl={2} mt={-0.5}>
              <Time type="interRegular14">{dayjs(date).format('D MMMM in H:mm')}</Time>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
    <Box pt={1}>
      <Typography type="interRegular14">{feedback}</Typography>
    </Box>
    <Box pt={0.5}>
      <StyledRating size="small" name="customized-color" defaultValue={star} precision={0.5} readOnly />
    </Box>
  </Wrapper>
);
