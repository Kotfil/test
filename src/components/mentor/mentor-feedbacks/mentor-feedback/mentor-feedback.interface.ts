export interface FeedbackViewInterface {
  id: string;
  name: string;
  avatar: string;
  star: number;
  feedback: string;
  date: number;
}

export interface FeedbackProps {
  feedback: FeedbackViewInterface;
}
