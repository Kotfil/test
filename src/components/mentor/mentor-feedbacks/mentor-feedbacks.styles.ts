import { Select } from '@mui/material';
import styled from 'styled-components';

export const SelectFilter = styled(Select)`
  width: 100%;
  height: 40px;

  & > div {
    width: calc(100% - 88px);
  }
`;
