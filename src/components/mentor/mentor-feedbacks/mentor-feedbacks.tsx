import { Box, Grid, List, ListItem, MenuItem } from '@mui/material';

import { Icon } from '@component/icon/icon';
import { MentorSector } from '@component/mentor/common/sector/mentor-sector';
import { MentorFeedback } from '@component/mentor/mentor-feedbacks/mentor-feedback/mentor-feedback';
import { Pagination } from '@component/pagination/pagination';

import { mockFeedbackList } from '../../../mocks/feedback-list.mock';
import { MentorFeedbacksOptions, MentorFilterEnum } from './mentor-feedbacks.options';
import { SelectFilter } from './mentor-feedbacks.styles';

const IconComponent = () => <Icon type="Sort" />;

export const MentorFeedbacks = () => (
  <Box px={1.25} pb={4} pt={1}>
    <MentorSector title="Отзывы" icon="Question">
      <Grid item xs={12}>
        <SelectFilter value={MentorFilterEnum.New} IconComponent={IconComponent}>
          {MentorFeedbacksOptions.map(({ value, label }) => (
            <MenuItem value={value}>{label}</MenuItem>
          ))}
        </SelectFilter>
      </Grid>
    </MentorSector>
    <List>
      {mockFeedbackList.map(feedback => (
        <ListItem disablePadding key={feedback.id}>
          <MentorFeedback feedback={feedback} />
        </ListItem>
      ))}
    </List>
    <Grid container justifyContent="center" pt={2}>
      <Pagination count={6} siblingCount={0} />
    </Grid>
  </Box>
);
