import dayjs from 'dayjs';
import { useState } from 'react';
import Calendar from 'react-calendar';

import { MentorSector } from '@component/mentor/common/sector/mentor-sector';

import NextIcon from './assets/next.svg';
import PrevIcon from './assets/prev.svg';

const disabledDate = dayjs().subtract(1, 'day').startOf('day');

export const MentorCalendar = () => {
  const [value, onChange] = useState(dayjs().add(1, 'day').toDate());

  return (
    <MentorSector title="Календарь встреч" icon="Calendar" justify="center">
      <Calendar
        onChange={onChange}
        value={value}
        view="month"
        defaultView="month"
        maxDetail="month"
        minDetail="month"
        showNeighboringMonth={false}
        prevLabel={<PrevIcon />}
        nextLabel={<NextIcon />}
        navigationLabel={({ date, locale }) => date.toLocaleString(locale, { month: 'long' })}
        tileDisabled={({ date }) => disabledDate.isSame(dayjs(date))}
      />
    </MentorSector>
  );
};
