import { FC } from 'react';

import NextLink from 'next/link';

import { LinkA } from '@component/link/link.style';

interface Props {
  href: string;
}

export const Link: FC<Props> = ({ href, children }) => (
  <NextLink href={href}>
    <LinkA>{children}</LinkA>
  </NextLink>
);
