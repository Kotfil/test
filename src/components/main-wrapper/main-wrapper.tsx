import React, { FC, useContext } from 'react';
import { useSwipeable } from 'react-swipeable';

import { Footer } from '@component/footer/footer';
import { ContentWrapper, Wrapper } from '@component/main-wrapper/main-wrapper.style';
import { Navigation } from '@component/navigation/navigation';
import { NavigationContext, NavigationProvider } from '@component/navigation/navigation.context';

interface WrapProps {
  hasFooter?: boolean;
  isActive?: boolean;
}

const Wrap: FC<WrapProps> = ({ hasFooter = true, isActive = true, children }) => {
  const { setOpen } = useContext(NavigationContext);
  const handlers = useSwipeable({
    delta: 50,
    preventDefaultTouchmoveEvent: true,
    trackTouch: true,
    trackMouse: true,
    onSwipedRight: () => setOpen(true),
  });

  return (
    <Wrapper {...(isActive && handlers)}>
      <ContentWrapper py={5}>{children}</ContentWrapper>
      {hasFooter && <Footer />}
    </Wrapper>
  );
};

export const MainWrapper: FC<WrapProps> = ({ hasFooter, isActive, children }) => (
  <NavigationProvider>
    <Navigation />
    <Wrap hasFooter={hasFooter} isActive={isActive}>
      {children}
    </Wrap>
  </NavigationProvider>
);
