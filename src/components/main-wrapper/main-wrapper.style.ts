import { Box } from '@mui/material';
import styled from 'styled-components';

import { ColorEnum } from '@enum/color.enum';

const NAVIGATION_WRAPPER_HEIGHT = 60;
const WRAPPER_PADDING_HEIGHT = 40;

export const Wrapper = styled.main`
  display: flex;
  flex: 1;
  min-height: 100vh;
  flex-direction: column;
  padding-top: ${NAVIGATION_WRAPPER_HEIGHT - WRAPPER_PADDING_HEIGHT}px;
  margin: 0 auto;
`;

export const NavigationWrapper = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  z-index: 1;
  width: 100%;
  height: ${NAVIGATION_WRAPPER_HEIGHT}px;
  background-color: ${ColorEnum.White};
`;

export const ContentWrapper = styled(Box)`
  flex: 1;
`;
