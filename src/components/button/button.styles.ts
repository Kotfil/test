import MaterialButton from '@mui/material/Button';
import styled from 'styled-components';

import { Typography } from '@component/typography/typography';
import { ColorEnum } from '@enum/color.enum';

export const ButtonPrimaryStyle = styled(MaterialButton)`
  background: linear-gradient(180deg, #1d96bc7f 0%, #1565c000 71.61%), #1565c0 !important;

  :disabled {
    background: ${ColorEnum.LightGrey} !important;
  }
`;

export const SocialButton = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  box-sizing: border-box;
  width: 100%;
  height: 40px;
  background-color: ${ColorEnum.White};
  border: 1px solid ${ColorEnum.Grey};
  border-radius: 20px;
`;

export const SocialButtonText = styled(Typography)`
  padding-left: 12px;
  color: ${ColorEnum.Grey};
`;

export const SocialButtonBlue = styled(SocialButton)`
  background-color: #1877f2;
  border: none;

  ${SocialButtonText} {
    color: ${ColorEnum.White};
  }
`;

export const SocialButtonDarkBlue = styled(SocialButton)`
  background-color: #2867b2;
  border: none;

  ${SocialButtonText} {
    color: ${ColorEnum.White};
  }
`;
