import { Grid } from '@mui/material';
import styled from 'styled-components';

import { ColorEnum } from '@enum/color.enum';

interface ButtonItemWrapperProps {
  isActive: boolean;
}

export const ButtonWrapper = styled(Grid)`
  padding: 16px 0;
`;

export const ButtonItemWrapper = styled(Grid)<ButtonItemWrapperProps>`
  display: flex;
  flex: 1;
  box-shadow: 0 1px 0 0 ${({ isActive }) => (isActive ? ColorEnum.PrimaryColor : ColorEnum.SuperLightGrey)};
`;
