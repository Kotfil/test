import { TextButton } from '@component/button/button';

import { ButtonListInterface } from './button-list.interface';
import { ButtonItemWrapper, ButtonWrapper } from './button-list.styles';

export const ButtonList = ({ options, activeType, onClick }: ButtonListInterface) => (
  <ButtonWrapper container p={0}>
    {options.map(({ type, label }) => (
      <ButtonItemWrapper item key={type} isActive={activeType === type} xs={6}>
        <TextButton color={activeType === type ? 'primary' : 'secondary'} onClick={onClick(type)}>
          {label}
        </TextButton>
      </ButtonItemWrapper>
    ))}
  </ButtonWrapper>
);
