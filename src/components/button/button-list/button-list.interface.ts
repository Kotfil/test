import { OnEventType } from '@type/on-event.type';

export interface ButtonListItemInterface {
  type: string;
  label: string;
}

export interface ButtonListInterface {
  activeType: string;
  options: ButtonListItemInterface[];
  onClick: OnEventType<string, OnEventType>;
}
