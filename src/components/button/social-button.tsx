import { FC } from 'react';

import {
  SocialButton,
  SocialButtonBlue,
  SocialButtonDarkBlue,
  SocialButtonText,
} from '@component/button/button.styles';

import FacebookIcon from './assets/facebook.svg';
import GoogleIcon from './assets/google.svg';

export const ButtonGoogle: FC = ({ children }) => (
  <SocialButton>
    <GoogleIcon />
    <SocialButtonText type="interRegular16">{children}</SocialButtonText>
  </SocialButton>
);

export const ButtonFacebook: FC = ({ children }) => (
  <SocialButtonBlue>
    <FacebookIcon />
    <SocialButtonText type="interRegular16">{children}</SocialButtonText>
  </SocialButtonBlue>
);

export const ButtonLinkedin: FC = ({ children }) => (
  <SocialButtonDarkBlue>
    <FacebookIcon />
    <SocialButtonText type="interRegular16">{children}</SocialButtonText>
  </SocialButtonDarkBlue>
);
