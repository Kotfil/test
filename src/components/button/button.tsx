import MaterialButton, { ButtonTypeMap } from '@mui/material/Button';
import { ExtendButtonBase } from '@mui/material/ButtonBase';
import { FontThemeStyle } from '@style/theme-styles/font.theme-style';

import { ButtonPrimaryStyle } from './button.styles';

type ButtonProps = {
  typography?: keyof typeof FontThemeStyle;
};

// @ts-ignore
export const Button: ExtendButtonBase<ButtonTypeMap<ButtonProps>> = ({
  typography,
  style,
  Component = MaterialButton,
  ...props
}) => {
  const buttonStyle =
    typography !== undefined
      ? {
          ...style,
          ...FontThemeStyle[typography],
        }
      : style;

  return <Component style={buttonStyle} {...props} />;
};

export const TextButton: ExtendButtonBase<ButtonTypeMap<ButtonProps>> = props => (
  <Button variant="text" color="primary" {...props} />
);

export const IconButton: ExtendButtonBase<ButtonTypeMap<ButtonProps>> = props => (
  <Button variant="empty" color="primary" {...props} />
);

export const PrimaryButton: ExtendButtonBase<ButtonTypeMap<ButtonProps>> = props => (
  <Button Component={ButtonPrimaryStyle} variant="contained" color="primary" {...props} />
);

export const SecondaryButton: ExtendButtonBase<ButtonTypeMap<ButtonProps>> = props => (
  <Button variant="transparent" color="secondary" {...props} />
);
