import { FontThemeStyle } from '@style/theme-styles/font.theme-style';
import { FC } from 'react';
import styled from 'styled-components';

type TypographyProps = {
  tag?: string;
  color?: string;
  type?: keyof typeof FontThemeStyle;
};

export const TypographyStyle = styled.span(({ type = 'interRegular16' }: TypographyProps) => FontThemeStyle[type]);

export const Typography: FC<TypographyProps> = props => <TypographyStyle {...props} as={props.tag as any} />;
