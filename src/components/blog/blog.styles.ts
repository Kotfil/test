import styled from 'styled-components';

import { ColorEnum } from '@enum/color.enum';

export const Separator = styled.div`
  height: 0;
  padding: 8px 0;
  border-bottom: 1px solid ${ColorEnum.LightGrey};
`;
