import { BlogBody } from '@component/blog/blog-list/blog-body/blog-body';
import { BlogItem } from '@component/blog/blog-list/blog-item/blog-item';

import { mockBlogList } from '../../../mocks/blog-list.mock';

export const BlogList = () => (
  <>
    {mockBlogList.map(blog => (
      <>
        <BlogItem key={blog.id} blog={blog} />
        <BlogBody blog={blog} />
      </>
    ))}
  </>
);
