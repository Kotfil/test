import { Grid } from '@mui/material';

import { BlogImage } from '@component/blog/blog-list/blog-item/blog-view/blog-title/blog-title.styles';
import { TextDivider } from '@component/text-divider/text-divider';
import { Typography } from '@component/typography/typography';
import { ColorEnum } from '@enum/color.enum';

interface BlogTitleProps {
  image: string;
  title: string;
  subtitle: string;
}

export const BlogTitle = ({ image, title, subtitle }: BlogTitleProps) => (
  <Grid container justifyContent="center" textAlign="center" xs={12}>
    <BlogImage src={image} />
    <Grid item xs={12} pt={2}>
      <Typography type="interMedium20">{title ?? ''}</Typography>
    </Grid>
    <Grid container py={2} xs={12} md={12} sx={{ textAlign: 'center' }}>
      <TextDivider color={ColorEnum.PrimaryColor}>{subtitle ?? ''}</TextDivider>
    </Grid>
  </Grid>
);
