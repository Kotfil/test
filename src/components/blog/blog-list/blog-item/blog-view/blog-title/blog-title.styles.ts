import styled from 'styled-components';

export const BlogImage = styled.img`
  display: block;
  width: 290px;
  height: 160px;
  object-fit: cover;
  border-radius: 8px;
`;
