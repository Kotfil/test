import { BlogTitle } from '@component/blog/blog-list/blog-item/blog-view/blog-title/blog-title';
import { BlogProps } from '@component/blog/blog-list/blog-item/blog-view/blog-view.interface';

export const BlogView = ({ blog: { image, title, subtitle } }: BlogProps) => (
  <BlogTitle image={image} title={title} subtitle={subtitle} />
);
