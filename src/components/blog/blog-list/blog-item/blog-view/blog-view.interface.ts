export interface BlogViewInterface {
  id: string;
  image: string;
  title: string;
  subtitle: string;
  date: number;
  readingDate: number;
  firstName: string;
  lastName: string;
  text: string;
}

export interface BlogProps {
  blog: BlogViewInterface;
}
