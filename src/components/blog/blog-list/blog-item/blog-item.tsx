import { BlogView } from '@component/blog/blog-list/blog-item/blog-view/blog-view';
import { BlogViewInterface } from '@component/blog/blog-list/blog-item/blog-view/blog-view.interface';
import { Link } from '@component/link/link';

interface BlogItemProps {
  blog: BlogViewInterface;
}

export const BlogItem = ({ blog }: BlogItemProps) => (
  <Link href={`blog/${blog.id}`}>
    <BlogView blog={blog} />
  </Link>
);
