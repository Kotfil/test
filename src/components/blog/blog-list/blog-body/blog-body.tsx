import { Grid } from '@mui/material';

import { BlogInfo } from '@component/blog/blog-list/blog-body/blog-info/blog-info';
import { BlogTime } from '@component/blog/blog-list/blog-body/blog-time/blog-time';
import { BlogProps } from '@component/blog/blog-list/blog-item/blog-view/blog-view.interface';

export function BlogBody({ blog: { firstName, lastName, text, date, readingDate } }: BlogProps) {
  return (
    <Grid py={2}>
      <BlogTime firstName={firstName} lastName={lastName} date={date} readingDate={readingDate} />
      <BlogInfo text={text} />
    </Grid>
  );
}
