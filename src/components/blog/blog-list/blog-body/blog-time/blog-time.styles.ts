import styled from 'styled-components';

export const PastTimeWrapper = styled.div`
  &:before {
    content: '';
    width: 20px;
    height: 20px;
    background: blue;
  }
`;

export const IconTime = styled.i`
  & {
    box-sizing: border-box;
    position: relative;
    display: block;
    transform: scale(var(--ggs, 1));
    width: 18px;
    height: 18px;
    border-radius: 100%;
    border: 2px solid transparent;
    box-shadow: 0 0 0 2px currentColor;
  }
  &::after {
    content: '';
    display: block;
    box-sizing: border-box;
    position: absolute;
    width: 7px;
    height: 7px;
    border-left: 2px solid;
    border-bottom: 2px solid;
    top: 1px;
    left: 5px;
  }
`;

export const PastTime = styled.div`
  &:before {
    content: '';
    display: block;
  }
`;
