import { Grid } from '@mui/material';
import dayjs from 'dayjs';

import { useRouter } from 'next/router';

import { IconTime } from '@component/blog/blog-list/blog-body/blog-time/blog-time.styles';
import { Typography } from '@component/typography/typography';
import { ColorEnum } from '@enum/color.enum';

interface BlogTimeProps {
  firstName: string;
  lastName: string;
  date: number;
  readingDate: number;
}

export function BlogTime({ firstName, lastName, date, readingDate }: BlogTimeProps) {
  const { query } = useRouter();
  return (
    <Grid sx={{ width: '100%', color: ColorEnum.Grey, direction: 'column' }} item xs={12} pt={1}>
      <>
        <Typography type="interRegular14">{`${dayjs(date).format(
          'D MMMM in H:mm',
        )} | ${firstName} ${lastName}`}</Typography>
      </>
      {query.id && (
        <Grid container sx={{ alignItems: 'center' }} pt={1}>
          <Grid item>
            <IconTime />
          </Grid>
          <Grid item sx={{ paddingLeft: 1 }}>
            <Typography type="interRegular14">{`${dayjs(readingDate).format('mm')} мин.`}</Typography>
          </Grid>
        </Grid>
      )}
    </Grid>
  );
}
