import { Grid } from '@mui/material';
import { useState } from 'react';

import { useRouter } from 'next/router';

import { Separator } from '@component/blog/blog.styles';
import { Button } from '@component/button/button';
import { Icon } from '@component/icon/icon';
import { Typography } from '@component/typography/typography';

interface BlogInfoProps {
  text: string;
}

export function BlogInfo({ text }: BlogInfoProps) {
  const blogId = useRouter().query.id;
  const [show, setShow] = useState<Boolean>(false);

  const selector = () => (show || blogId ? text : `${text.substring(0, 172)}...`);

  const buttonsRendering = (blogId && 'right') || 'space-between';
  return (
    <Grid container>
      <Grid item pt={2}>
        <Typography type="interMedium20">{selector()}</Typography>
      </Grid>
      <Grid container justifyContent={buttonsRendering} alignItems="center">
        {!blogId && (
          <Grid item>
            <Button onClick={() => setShow(!show)}>Еще ...</Button>
          </Grid>
        )}
        <Grid item pt={0.5}>
          <Icon type="Link" />
        </Grid>
      </Grid>
      <Grid item sx={{ width: '100%' }}>
        <Separator />
      </Grid>
    </Grid>
  );
}
