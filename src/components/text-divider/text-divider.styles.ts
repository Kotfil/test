import { Grid } from '@mui/material';
import styled from 'styled-components';

import { Typography } from '@component/typography/typography';
import { ColorEnum } from '@enum/color.enum';

export const Container = styled(Grid)`
  position: relative;
  height: 16px;
  padding-top: 8px;
`;

export const Divider = styled.div`
  width: 100%;
  height: 1px;
  background-color: ${ColorEnum.LightGrey};
`;

export const Title = styled(Typography)<{ color: string }>`
  position: absolute;
  top: 50%;
  left: 50%;
  padding: 0 16px;
  color: ${({ color }) => (color ? color : ColorEnum.MainText)};
  background-color: ${ColorEnum.White};
  transform: translateX(-50%) translateY(-50%);
`;
