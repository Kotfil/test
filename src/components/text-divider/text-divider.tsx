import { Container, Divider, Title } from './text-divider.styles';

interface TextDividerProps {
  color?: string;
  children?: string;
}

export const TextDivider = ({ children, color }: TextDividerProps) => {
  return (
    <Container item xs={12}>
      <Divider />
      <Title color={color} type="robotoRegular14">
        {children}
      </Title>
    </Container>
  );
};
