import { Pagination as MuiPagination, PaginationItem } from '@mui/material';
import { PaginationProps } from '@mui/material/Pagination/Pagination';

import Left from './assests/left.svg';
import Right from './assests/right.svg';
import { useStyles } from './pagination.styles';

export const Pagination = (props: PaginationProps) => {
  const classes = useStyles();

  return (
    <MuiPagination
      {...props}
      renderItem={item => (
        <PaginationItem
          components={{ previous: Left, next: Right }}
          classes={{ selected: classes.selected, text: classes.text }}
          {...item}
        />
      )}
    />
  );
};
