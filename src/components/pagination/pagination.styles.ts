import { makeStyles } from '@mui/styles';

import { ColorEnum } from '@enum/color.enum';

export const useStyles = makeStyles(() => ({
  selected: {
    backgroundColor: `${ColorEnum.PrimaryColor} !important`,
    color: `${ColorEnum.White} !important`,
  },
  text: {
    color: ColorEnum.LightGrey2,
    fontFamily: 'Inter',
    fontSize: 16,
  },
}));
