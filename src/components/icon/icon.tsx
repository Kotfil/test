import Icons, { IconEnum } from './icon.enum';

interface IconProps {
  type: IconEnum;
}

export const Icon = ({ type }: IconProps) => {
  const Component = Icons[type];

  return <Component />;
};
