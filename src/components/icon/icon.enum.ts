import Back from './assets/back.svg';
import Blog from './assets/blog.svg';
import Calendar from './assets/calendar.svg';
import Confident from './assets/confident.svg';
import Description from './assets/description.svg';
import Expand from './assets/expand.svg';
import Facebook from './assets/facebook.svg';
import Filter from './assets/filter.svg';
import Format from './assets/format.svg';
import Hamburger from './assets/hamburger.svg';
import Home from './assets/home.svg';
import Info from './assets/info.svg';
import Instagram from './assets/instagram.svg';
import LangInfo from './assets/lang-info.svg';
import Language from './assets/language.svg';
import Link from './assets/link.svg';
import LinkedInFill from './assets/linkendin-fill.svg';
import LinkedIn from './assets/linkendin.svg';
import Professional from './assets/professional.svg';
import Question from './assets/question.svg';
import Search from './assets/search.svg';
import Send from './assets/send.svg';
import Services from './assets/services.svg';
import Sort from './assets/sort.svg';
import Support from './assets/support.svg';
import Twitter from './assets/twitter.svg';
import Uses from './assets/uses.svg';

const icons = {
  Back,
  Blog,
  Calendar,
  Confident,
  Description,
  Expand,
  Facebook,
  Filter,
  Format,
  Hamburger,
  Home,
  Info,
  Instagram,
  LangInfo,
  Language,
  Link,
  LinkedIn,
  LinkedInFill,
  Professional,
  Question,
  Search,
  Send,
  Services,
  Sort,
  Support,
  Twitter,
  Uses,
};

export type IconEnum = keyof typeof icons;

export default icons;
