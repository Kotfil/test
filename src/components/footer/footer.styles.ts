import styled from 'styled-components';

import { Typography } from '@component/typography/typography';
import { ColorEnum } from '@enum/color.enum';

export const Wrapper = styled.footer`
  background-color: ${ColorEnum.PrimaryColor};
`;

export const Title = styled(Typography)`
  margin: 0 auto;
  color: ${ColorEnum.White};
`;

export const Link = styled.a`
  color: ${ColorEnum.White};

  &:not(:first-child) {
    margin-top: 16px;
  }
`;

export const License = styled(Typography)`
  color: ${ColorEnum.LightGrey};
`;
