import { Box, Grid } from '@mui/material';

import { SocialIcons } from '@component/social/socialIcons';

import { License, Title, Wrapper } from './footer.styles';

export const Footer = () => {
  return (
    <Wrapper>
      <Grid container pt={4} direction="row" justifyContent="center" alignItems="center">
        <Grid item>
          <Title type="syncopateBold18">Besttera</Title>
        </Grid>
      </Grid>
      <Grid container pt={2} px={2}>
        <Grid item xs={6} pr={2}>
          <Grid item xs={12}>
            <Title type="robotoRegular12">О нас</Title>
          </Grid>
          <Grid item xs={12}>
            <Title type="robotoRegular12">FAQ</Title>
          </Grid>
          <Grid item xs={12}>
            <Title type="robotoRegular12">Условия использования</Title>
          </Grid>
        </Grid>
        <Grid item xs={6} pl={3}>
          <Grid item xs={12}>
            <Title type="robotoRegular12">Блог</Title>
          </Grid>
          <Grid item xs={12}>
            <Title type="robotoRegular12">Помощь</Title>
          </Grid>
          <Grid item xs={12}>
            <Title type="robotoRegular12">Политика конфиденциальности</Title>
          </Grid>
          <SocialIcons />
        </Grid>
      </Grid>
      <Box pl={2} py={1}>
        <License type="robotoRegular10">BestTechEra LLC. 2022. All rights reserved.</License>
      </Box>
    </Wrapper>
  );
};
