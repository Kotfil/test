import { InputAdornment, TextField } from '@mui/material';
import { useState } from 'react';

import { Icon } from '@component/icon/icon';

export const InputSearch = () => {
  const [value, setValue] = useState('');

  const handleChange = event => setValue(event.target.value);

  return (
    <TextField
      type="text"
      value={value}
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">
            <Icon type="Search" />
          </InputAdornment>
        ),
      }}
      label={value.length > 0 ? 'Поиск фрилансов' : undefined}
      placeholder="Поиск фрилансов"
      onChange={handleChange}
      size="small"
      fullWidth
    />
  );
};
