import { useField } from 'formik';
import { FC, InputHTMLAttributes } from 'react';

import { IconWrapper, InputStyle, Wrapper } from '@component/input/input.styles';

export interface InputProps extends InputHTMLAttributes<any> {
  icon?: any;
  name: string;
}

export const Input: FC<InputProps> = ({ name, icon: Icon, ...props }) => {
  const [field] = useField(name);

  return (
    <Wrapper>
      {Icon !== undefined && (
        <IconWrapper>
          <Icon />
        </IconWrapper>
      )}
      <InputStyle {...field} {...props} />
    </Wrapper>
  );
};
