import { Grid } from '@mui/material';

import { ButtonFacebook, ButtonGoogle, ButtonLinkedin } from '@component/button/social-button';
import { Icon } from '@component/icon/icon';

export const SocialIcons = () => (
  <Grid container pt={1}>
    <Grid item xs={3}>
      <Icon type="Facebook" />
    </Grid>
    <Grid item xs={3}>
      <Icon type="Twitter" />
    </Grid>
    <Grid item xs={3}>
      <Icon type="LinkedIn" />
    </Grid>
    <Grid item xs={3}>
      <Icon type="Instagram" />
    </Grid>
  </Grid>
);

export const SocialButtons = () => (
  <>
    <Grid item xs={12} pt={2}>
      <ButtonGoogle>Продолжить с Google</ButtonGoogle>
    </Grid>
    <Grid item xs={12} pt={2}>
      <ButtonFacebook>Продолжить с Facebook</ButtonFacebook>
    </Grid>
    <Grid item xs={12} pt={2}>
      <ButtonLinkedin>Продолжить с LinkedIn</ButtonLinkedin>
    </Grid>
  </>
);
