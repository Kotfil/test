import { Box } from '@mui/material';
import { useCallback, useState } from 'react';

import { ButtonList } from '@component/button/button-list/button-list';
import { Categories } from '@component/categories/categories';
import { Mentors } from '@component/mentors/mentors';

import { mentorTypeList } from './main-mentors.options';

export const MainMentors = () => {
  const [activeType, setActiveType] = useState(mentorTypeList[0].type);

  const handleType = useCallback((type: string) => () => setActiveType(type), []);

  return (
    <Box pt={2} px={2}>
      <ButtonList options={mentorTypeList} activeType={activeType} onClick={handleType} />
      <Mentors />
      <Categories type={activeType} />
    </Box>
  );
};
