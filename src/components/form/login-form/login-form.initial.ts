import { object, string } from 'yup';

export const loginFormValidation = object({
  email: string().email(),
  password: string().required(),
});

export const initialLoginForm = {
  email: '',
  password: '',
};
