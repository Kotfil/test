import { Grid } from '@mui/material';
import { Formik, Form, useFormikContext } from 'formik';

import NextLink from 'next/link';

import { PrimaryButton } from '@component/button/button';
import { InputForm, LostPassword } from '@page-style/login/login.style';

import { initialLoginForm, loginFormValidation } from './login-form.initial';

const FormikView = () => {
  const { dirty, isValid } = useFormikContext();

  return (
    <Form>
      <Grid item xs={12} pt={2}>
        <InputForm name="email" placeholder="Email" type="email" />
      </Grid>
      <Grid item xs={12} pt={2}>
        <InputForm name="password" placeholder="Пароль" type="password" />
      </Grid>
      <Grid item xs={12} pt={1.5} justifyContent="center">
        <NextLink href="/reset-password">
          <LostPassword type="interRegular14">Забыли пароль?</LostPassword>
        </NextLink>
      </Grid>
      <Grid item xs={12} pt={2.25} justifyContent="center">
        <PrimaryButton disabled={!(dirty && isValid)} type="submit">
          Вход
        </PrimaryButton>
      </Grid>
    </Form>
  );
};

export const LoginForm = () => {
  const handleSubmit = () => {};

  return (
    <Formik initialValues={initialLoginForm} validationSchema={loginFormValidation} onSubmit={handleSubmit}>
      <FormikView />
    </Formik>
  );
};
