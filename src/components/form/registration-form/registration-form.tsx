import { Grid } from '@mui/material';
import { OnEventType } from '@type/on-event.type';
import { Form, Formik, useFormikContext } from 'formik';

import { PrimaryButton } from '@component/button/button';
import { Checkbox } from '@component/checkbox/checkbox';
import { InputForm } from '@page-style/login/login.style';

import { initialRegistrationForm, registrationFormValidation } from './registration-form.initial';

interface RegistrationFormProps {
  onSubmit: OnEventType<any>;
}

const FormView = () => {
  const { dirty, isValid } = useFormikContext();

  return (
    <Form>
      <Grid item xs={12} pt={2}>
        <InputForm name="email" placeholder="Email" type="email" />
      </Grid>
      <Grid item xs={12} pt={2}>
        <InputForm name="password" placeholder="Пароль" type="password" />
      </Grid>
      <Grid item xs={12} pt={2}>
        <InputForm name="passwordConfirmation" placeholder="Повторите пароль" type="password" />
      </Grid>
      <Grid item xs={12} pt={1.5} justifyContent="center">
        <Checkbox name="isAgree" label="Ознакомлен и принимаю условия Пользовательского соглашения" />
      </Grid>
      <Grid item xs={12} pt={2.25} justifyContent="center">
        <PrimaryButton disabled={!(dirty && isValid)} type="submit">
          Продолжить
        </PrimaryButton>
      </Grid>
    </Form>
  );
};

export const RegistrationForm = ({ onSubmit }: RegistrationFormProps) => (
  <Formik initialValues={initialRegistrationForm} validationSchema={registrationFormValidation} onSubmit={onSubmit}>
    <FormView />
  </Formik>
);
