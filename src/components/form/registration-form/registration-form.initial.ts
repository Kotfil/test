import { object, string, boolean, ref } from 'yup';

export const registrationFormValidation = object({
  email: string().email(),
  password: string().required(),
  passwordConfirmation: string()
    .required()
    .oneOf([ref('password')], 'Passwords does not match'),
  isAgree: boolean()
    .required('The terms and conditions must be accepted.')
    .oneOf([true], 'The terms and conditions must be accepted.'),
});

export const initialRegistrationForm = {
  email: '',
  password: '',
  passwordConfirmation: '',
  isAgree: false,
};
