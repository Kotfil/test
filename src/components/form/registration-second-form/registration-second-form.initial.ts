import { object, string } from 'yup';

export const registrationSecondFormValidation = object({
  name: string().required(),
  lastName: string().required(),
});

export const initialRegistrationSecond = {
  name: '',
  lastName: '',
  type: 'professional',
};
