import styled from 'styled-components';

import { TypographyStyle } from '@component/typography/typography';
import { ColorEnum } from '@enum/color.enum';

interface TypeButtonProps {
  isLeft: boolean;
  isActive: boolean;
}

export const TypeButton = styled.button<TypeButtonProps>`
  width: 100%;
  border: 1px solid ${({ isActive }) => (isActive ? ColorEnum.PrimaryColor : ColorEnum.LightGrey)};
  border-radius: ${({ isLeft }) => (isLeft ? '6px 0 0 6px' : '0 6px 6px 0')};
  background-color: ${({ isActive }) => (isActive ? ColorEnum.PrimaryColor : ColorEnum.White)};
  padding: 8px 0;

  ${TypographyStyle} {
    color: ${({ isActive }) => (isActive ? ColorEnum.White : ColorEnum.Grey)};
  }
`;
