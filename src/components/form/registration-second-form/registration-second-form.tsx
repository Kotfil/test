import { Grid } from '@mui/material';
import { OnEventType } from '@type/on-event.type';
import { Form, Formik, useField, useFormikContext } from 'formik';

import { PrimaryButton } from '@component/button/button';
import { TypeButton } from '@component/form/registration-second-form/registration-second-form.style';
import { Typography } from '@component/typography/typography';
import { InputForm } from '@page-style/login/login.style';

import { initialRegistrationSecond, registrationSecondFormValidation } from './registration-second-form.initial';

interface RegistrationFormProps {
  onSubmit: OnEventType<any>;
}

const FormView = () => {
  const { dirty, isValid } = useFormikContext();
  const [{ value }, , { setValue }] = useField('type');

  const handleType = (type: string) => () => setValue(type);

  return (
    <Form>
      <Grid item xs={12} pt={2}>
        <InputForm name="name" placeholder="Имя" type="text" />
      </Grid>
      <Grid item xs={12} pt={2}>
        <InputForm name="lastName" placeholder="Фамилия" type="text" />
      </Grid>
      <Grid item xs={12} pt={4}>
        <Typography type="interRegular16">Регистрируюсь в качестве:</Typography>
      </Grid>
      <Grid item xs={12} pt={1}>
        <Grid container direction="row">
          <Grid item xs={6}>
            <TypeButton type="button" isLeft isActive={value == 'professional'} onClick={handleType('professional')}>
              <Typography type="nunitoMedium18">Профессионал</Typography>
            </TypeButton>
          </Grid>
          <Grid item xs={6}>
            <TypeButton type="button" isLeft={false} isActive={value == 'client'} onClick={handleType('client')}>
              <Typography type="interRegular14">Клиент</Typography>
            </TypeButton>
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={12} pt={6} justifyContent="center">
        <PrimaryButton disabled={!(dirty && isValid)} type="submit">
          Продолжить
        </PrimaryButton>
      </Grid>
    </Form>
  );
};

export const RegistrationSecondForm = ({ onSubmit }: RegistrationFormProps) => (
  <Formik
    initialValues={initialRegistrationSecond}
    validationSchema={registrationSecondFormValidation}
    onSubmit={onSubmit}
  >
    <FormView />
  </Formik>
);
