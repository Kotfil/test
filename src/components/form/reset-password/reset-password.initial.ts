import { object, string } from 'yup';

export const resetFormValidation = object({
  email: string().email(),
});

export const initialResetForm = {
  email: '',
};
