import { Grid } from '@mui/material';
import { OnEventType } from '@type/on-event.type';
import { Formik, Form, useFormikContext } from 'formik';

import { PrimaryButton } from '@component/button/button';
import { Typography } from '@component/typography/typography';
import { InputForm } from '@page-style/login/login.style';

import { initialResetForm, resetFormValidation } from './reset-password.initial';

interface ResetPasswordFormProps {
  onSubmit: OnEventType<any>;
}

const FormikView = () => {
  const { dirty, isValid } = useFormikContext();

  return (
    <Form>
      <Grid item xs={12} pt={2}>
        <InputForm name="email" placeholder="Email" type="email" />
      </Grid>
      <Grid item xs={12} pt={2} justifyContent="center">
        <Typography type="interRegular16">
          На указаный адрес электронной почты придет контрольное письмо с инструкциями по восстановлению пароля
        </Typography>
      </Grid>
      <Grid item xs={12} pt={4} justifyContent="center">
        <PrimaryButton disabled={!(dirty && isValid)} type="submit">
          Продолжить
        </PrimaryButton>
      </Grid>
    </Form>
  );
};

export const ResetPassword = ({ onSubmit }: ResetPasswordFormProps) => (
  <Formik initialValues={initialResetForm} validationSchema={resetFormValidation} onSubmit={onSubmit}>
    <FormikView />
  </Formik>
);
