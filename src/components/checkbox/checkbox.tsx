import { FormControlLabel } from '@mui/material';
import MuiCheckbox from '@mui/material/Checkbox';
import { useField } from 'formik';

import { Typography } from '@component/typography/typography';

interface CheckboxProps {
  name: string;
  label: string;
}

export const Checkbox = ({ label, name }: CheckboxProps) => {
  const [field] = useField(name);

  return (
    <FormControlLabel
      control={<MuiCheckbox {...field} />}
      label={<Typography type="interRegular14">{label}</Typography>}
    />
  );
};
