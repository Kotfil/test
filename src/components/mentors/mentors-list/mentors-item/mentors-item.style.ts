import styled from 'styled-components';

import { ColorEnum } from '@enum/color.enum';

export const Wrapper = styled.li`
  display: flex;
  flex-direction: column;
  padding-top: 16px;
`;

export const CategoryWrapper = styled.ul`
  display: flex;
  flex-direction: row;
`;

export const Category = styled.li`
  padding: 4px 8px;
  color: ${ColorEnum.MiddleGrey};
  font-weight: 400;
  font-size: 14px;
  font-family: 'Roboto', sans-serif;
  line-height: 16px;
  border: 1px solid ${ColorEnum.LightGrey2};
  border-radius: 6px;

  &:not(:first-child) {
    margin-left: 4px;
  }
`;

export const MentorWrapper = styled.div`
  display: flex;
  padding-bottom: 16px;
`;

export const MentorAvatar = styled.img`
  flex: 0 0 70px;
  width: 70px;
  height: 70px;
  object-fit: cover;
  border-radius: 50%;
`;

export const MentorInfo = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  margin-left: 16px;
`;

export const MentorName = styled.p`
  font-weight: 400;
  font-size: 14px;
  font-family: 'Roboto', sans-serif;
  line-height: 16px;
`;

export const RatingWrapper = styled.p`
  display: flex;
  justify-content: start;
  margin-top: 4px;
`;

export const Vote = styled.span`
  margin-left: 8px;
  color: ${ColorEnum.MiddleGrey};
  font-weight: 500;
  font-size: 12px;
  font-family: 'Roboto', sans-serif;
  line-height: 15px;
`;

export const MentorInfoBlock = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: 8px;
`;

export const MentorInfoWrapper = styled.p`
  display: flex;
  flex-direction: column;
`;

export const LocationTitle = styled.span`
  color: ${ColorEnum.MiddleGrey};
  font-weight: 500;
  font-size: 12px;
  font-family: 'Roboto', sans-serif;
  line-height: 15px;
`;

export const Location = styled.span`
  color: ${ColorEnum.MainText};
  font-weight: 500;
  font-size: 12px;
  font-family: 'Roboto', sans-serif;
  line-height: 15px;
`;
