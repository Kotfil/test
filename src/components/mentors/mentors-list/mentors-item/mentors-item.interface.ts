export interface MentorTag {
  id: string;
  name: string;
}

export interface MentorsItemInterface {
  name: string;
  tags: MentorTag[];
  avatar: string;
  star: number;
  vote: number;
  location: string;
  price: number;
  experience: number;
}
