import { Link } from '@component/link/link';
import { MentorView } from '@component/mentor/mentor-view/mentor-view';
import { MentorsViewInterface } from '@component/mentor/mentor-view/mentor-view.interface';
import { Separator } from '@component/mentors/mentors.styles';

interface MentorsItemProps {
  mentor: MentorsViewInterface;
}

export const MentorsItem = ({ mentor }: MentorsItemProps) => (
  <>
    <Separator />
    <Link href={`/mentor/${mentor.id}`}>
      <MentorView mentor={mentor} />
    </Link>
  </>
);
