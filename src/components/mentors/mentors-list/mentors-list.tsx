import { Box } from '@mui/material';

import { MentorsItem } from '@component/mentors/mentors-list/mentors-item/mentors-item';
import { Separator } from '@component/mentors/mentors.styles';

import { mockMentorsList } from '../../../mocks/mentors-list.mock';

export const MentorsList = () => (
  <Box>
    {mockMentorsList.map(mentor => (
      <MentorsItem key={mentor.id} mentor={mentor} />
    ))}
    <Separator />
  </Box>
);
