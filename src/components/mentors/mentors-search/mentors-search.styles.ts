import { Grid } from '@mui/material';
import styled from 'styled-components';

export const SearchWrapper = styled(Grid)`
  flex: 1;
`;
