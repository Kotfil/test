import { Grid, IconButton } from '@mui/material';

import { Icon } from '@component/icon/icon';
import { InputSearch } from '@component/input/input-search';

import { SearchWrapper } from './mentors-search.styles';

export const MentorsSearch = () => (
  <Grid container>
    <SearchWrapper item>
      <InputSearch />
    </SearchWrapper>
    <Grid item xs="auto">
      <IconButton>
        <Icon type="Filter" />
      </IconButton>
    </Grid>
  </Grid>
);
