import { Box, Grid } from '@mui/material';

import NextLink from 'next/link';

import { PrimaryButton } from '@component/button/button';
import { MentorsList } from '@component/mentors/mentors-list/mentors-list';

import { MentorsSearch } from './mentors-search/mentors-search';

export const Mentors = () => (
  <Grid pt={2}>
    <MentorsSearch />
    <MentorsList />
    <Box mt={5}>
      <NextLink href="/registration">
        <PrimaryButton>Стать консультантом</PrimaryButton>
      </NextLink>
    </Box>
  </Grid>
);
