import { SwitchProps } from '@mui/material';

import { SwitchStyled } from './switcher.style';

export const Switch = (props: SwitchProps) => (
  <SwitchStyled focusVisibleClassName=".Mui-focusVisible" disableRipple {...props} />
);
