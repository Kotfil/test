import { Avatar, Grid, Slider, TextareaAutosize } from '@mui/material';
import styled from 'styled-components';

import { SecondaryButton } from '@component/button/button';
import { Typography } from '@component/typography/typography';
import { ColorEnum } from '@enum/color.enum';

export const UserAvatar = styled(Avatar)`
  border: 3px solid ${ColorEnum.PrimaryColor};
`;

export const ChangeButton = styled(SecondaryButton)`
  height: 24px;
  max-width: 200px;
  padding: 8px 16px !important;
`;

export const GridForm = styled(Grid)`
  width: 100%;
`;

export const Textarea = styled(TextareaAutosize)`
  width: 100%;
  padding: 16px;
  border: 1px solid #e0e0e0;
  box-sizing: border-box;
  border-radius: 4px;
  ::-webkit-resizer {
    display: none;
  }
`;

export const TypographyLinked = styled(Typography)`
  display: flex;
  align-items: center;
`;

export const TitleLinked = styled.span`
  margin-left: 8px;
`;

export const ExperienceSlider = styled(Slider)({
  height: 4,
  padding: '15px 0',
  '& .MuiSlider-thumb': {
    height: 16,
    width: 16,
    border: '1px solid white',
    backgroundColor: ColorEnum.PrimaryColor,
  },
  '& .MuiSlider-valueLabel': {
    color: ColorEnum.Black,
    fontSize: 14,
    fontWeight: 'normal',
    top: 50,
    backgroundColor: 'unset',
    '&:before': { display: 'none' },
  },
  '& .MuiSlider-markLabel': {
    opacity: 0.3,
    marginTop: 4,
    '&[data-index="1"].MuiSlider-markLabelActive': {
      transform: 'translateX(-8px)',
    },
  },
});
