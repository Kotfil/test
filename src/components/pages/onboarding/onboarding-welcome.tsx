import { Grid } from '@mui/material';
import { OnEventType } from '@type/on-event.type';

import { PrimaryButton } from '@component/button/button';
import { MainWrapper } from '@component/main-wrapper/main-wrapper';
import { Typography } from '@component/typography/typography';
import { Title } from '@page-style/login/login.style';

interface OnboardingWelcomeProps {
  onClick: OnEventType;
}

export const OnboardingWelcome = ({ onClick }: OnboardingWelcomeProps) => (
  <MainWrapper hasFooter={false}>
    <Grid container px={1.75} pt={2}>
      <Grid item xs={12} pt={0.5} justifyContent="center">
        <Title type="interMedium20">Старт</Title>
      </Grid>
      <Grid item xs={12} pt={4}>
        <Typography type="interRegular16">
          Чтобы начать пользоваться Besttera необходимо добавить некоторые персональные данные.
        </Typography>
      </Grid>
      <Grid item xs={12} pt={4}>
        <Typography type="interRegular16">Заполненная информация будет отображена в вашем профиле.</Typography>
      </Grid>
      <Grid item xs={12} pt={4}>
        <Typography type="interRegular16">Вы всегда сможете вносить изменения.</Typography>
      </Grid>
      <Grid item xs={12} pt={6} justifyContent="center">
        <PrimaryButton onClick={onClick}>Продолжить</PrimaryButton>
      </Grid>
    </Grid>
  </MainWrapper>
);
