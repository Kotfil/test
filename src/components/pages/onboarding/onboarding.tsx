import {
  FormControl,
  FormControlLabel,
  Grid,
  List,
  ListItem,
  MenuItem,
  Radio,
  RadioGroup,
  Select,
  TextField,
} from '@mui/material';
import { Form, Formik } from 'formik';
import CurrencyInput from 'react-currency-input-field';

import { PrimaryButton } from '@component/button/button';
import { MainWrapper } from '@component/main-wrapper/main-wrapper';
import { MentorSector } from '@component/mentor/common/sector/mentor-sector';
import { Switch } from '@component/switcher/switcher';
import { Typography } from '@component/typography/typography';
import { ColorEnum } from '@enum/color.enum';
import { CategoryFilter } from '@page-style/category/category.styles';
import { InputForm } from '@page-style/login/login.style';

import Linkedin from './assets/linkedin.svg';
import {
  ChangeButton,
  ExperienceSlider,
  GridForm,
  Textarea,
  TitleLinked,
  TypographyLinked,
  UserAvatar,
} from './onboarding.styles';

const marks = [
  {
    value: 0,
    label: '0',
  },
  {
    value: 50,
    label: '50+',
  },
];

export const Onboarding = () => {
  const a = console;
  const handleChange = a.log;

  return (
    <MainWrapper hasFooter={false} isActive={false}>
      <Grid container pt={2} direction="column" alignItems="center">
        <Grid item>
          <UserAvatar
            sx={{ bgcolor: ColorEnum.PrimaryColor, width: 147, height: 147 }}
            src="https://randomuser.me/api/portraits/men/75.jpg"
          >
            H
          </UserAvatar>
        </Grid>
        <Grid item pt={1} justifyContent="center">
          <ChangeButton typography="nunitoMedium16">Изменить фото</ChangeButton>
        </Grid>
      </Grid>
      <Formik initialValues={{}} onSubmit={handleChange}>
        <Form>
          <Grid container px={2} pt={3}>
            <Grid item>
              <Typography type="nunitoMedium18">Основная информация</Typography>
            </Grid>
            <GridForm item py={1}>
              <InputForm name="name" placeholder="Имя" type="text" />
            </GridForm>
            <GridForm item py={1}>
              <InputForm name="surname" placeholder="Фамилия" type="text" />
            </GridForm>
            <GridForm item py={1}>
              <InputForm name="profession" placeholder="Профессия" type="text" />
            </GridForm>
            <GridForm item py={1}>
              <InputForm name="country" placeholder="Страна" type="text" />
            </GridForm>
            <GridForm item py={1}>
              <InputForm name="city" placeholder="Город" type="text" />
            </GridForm>
          </Grid>
          <Grid container px={2} pt={3}>
            <Grid item>
              <Typography type="nunitoMedium18">Опыт</Typography>
            </Grid>
            <GridForm item>
              <ExperienceSlider
                defaultValue={5}
                aria-label="Default"
                valueLabelDisplay="on"
                marks={marks}
                step={1}
                min={0}
                max={50}
              />
            </GridForm>
          </Grid>
          <Grid container px={2} pt={3} direction="column">
            <Grid item>
              <Typography type="nunitoMedium18">Тип занятости</Typography>
            </Grid>
            <Grid item>
              <FormControl>
                <RadioGroup defaultValue="freelancer" name="type">
                  <FormControlLabel value="freelancer" control={<Radio />} label="Фрилансер" />
                  <FormControlLabel value="employer" control={<Radio />} label="Сотрудник компании" />
                </RadioGroup>
              </FormControl>
            </Grid>
          </Grid>
          <Grid container px={2} pt={3} direction="column">
            <Grid item>
              <MentorSector title="Описание опыта" icon="Description">
                <Textarea minRows={3} placeholder="Ваш опыт" />
              </MentorSector>
            </Grid>
          </Grid>
          <Grid container px={2} pt={3} direction="column">
            <Grid item direction="column">
              <Grid item>
                <Typography type="nunitoMedium18">Ваши навыки</Typography>
              </Grid>
              <Grid item my={-1.25} mb={1}>
                <Typography type="interRegular10">Вы можете выбрать не более 5 основных навыков</Typography>
              </Grid>
            </Grid>
            <GridForm item py={1}>
              <InputForm name="main" placeholder="Введите основные навыки" type="text" />
            </GridForm>
            <Grid container justifyContent="space-between">
              <CategoryFilter item my={0.5}>
                <Typography type="interRegular16">Дизайн UI/UX</Typography>
              </CategoryFilter>
              <CategoryFilter item my={0.5}>
                <Typography type="interRegular16">Графика</Typography>
              </CategoryFilter>
              <CategoryFilter item my={0.5}>
                <Typography type="interRegular16">Figma</Typography>
              </CategoryFilter>
              <CategoryFilter item my={0.5}>
                <Typography type="interRegular16">Бизнес и дизайн</Typography>
              </CategoryFilter>
              <CategoryFilter item my={0.5}>
                <Typography type="interRegular16">Бизнес дизайн</Typography>
              </CategoryFilter>
            </Grid>
            <GridForm item pb={1} pt={3}>
              <InputForm name="secondary" placeholder="Введите дополнительные навыки" type="text" />
            </GridForm>
            <Grid container justifyContent="space-between">
              <CategoryFilter item my={0.5}>
                <Typography type="interRegular16">Дизайн UI/UX</Typography>
              </CategoryFilter>
              <CategoryFilter item my={0.5}>
                <Typography type="interRegular16">Графика</Typography>
              </CategoryFilter>
              <CategoryFilter item my={0.5}>
                <Typography type="interRegular16">Figma</Typography>
              </CategoryFilter>
              <CategoryFilter item my={0.5}>
                <Typography type="interRegular16">Бизнес и дизайн</Typography>
              </CategoryFilter>
              <CategoryFilter item my={0.5}>
                <Typography type="interRegular16">Бизнес дизайн</Typography>
              </CategoryFilter>
            </Grid>
          </Grid>
          <Grid container px={2} pt={3} direction="column">
            <Grid item>
              <Typography type="nunitoMedium18">Виды услуг</Typography>
            </Grid>
            <Grid item>
              <List sx={{ width: '100%' }}>
                <ListItem divider disablePadding>
                  <Grid container justifyContent="space-between" py={1}>
                    <Grid item>
                      <Typography type="interRegular14">Консультации</Typography>
                    </Grid>
                    <Grid item>
                      <Switch />
                    </Grid>
                  </Grid>
                </ListItem>
                <ListItem divider disablePadding>
                  <Grid container justifyContent="space-between" py={1}>
                    <Grid item>
                      <Typography type="interRegular14">Обучение</Typography>
                    </Grid>
                    <Grid item>
                      <Switch />
                    </Grid>
                  </Grid>
                </ListItem>
                <ListItem divider disablePadding>
                  <Grid container justifyContent="space-between" py={1}>
                    <Grid item>
                      <Typography type="interRegular14">Деловые знакомства</Typography>
                    </Grid>
                    <Grid item>
                      <Switch />
                    </Grid>
                  </Grid>
                </ListItem>
              </List>
            </Grid>
          </Grid>
          <Grid container px={2} pt={3} direction="column">
            <Grid item>
              <Typography type="nunitoMedium18">Выберите валюту</Typography>
            </Grid>
            <Grid item pt={0.5}>
              <Select value={20} sx={{ width: '100%' }} onChange={handleChange}>
                <MenuItem value={10}>USD</MenuItem>
                <MenuItem value={20}>UAH</MenuItem>
                <MenuItem value={30}>EUR</MenuItem>
              </Select>
            </Grid>
          </Grid>
          <Grid container px={2} pt={3} direction="column">
            <Grid item>
              <Typography type="nunitoMedium18">Стоимость консультации</Typography>
            </Grid>
            <Grid item pt={0.5}>
              <TextField
                sx={{ width: '100%' }}
                InputProps={{
                  inputComponent: CurrencyInput,
                  inputProps: {
                    name: 'price',
                    placeholder: 'Please enter a number',
                    prefix: '',
                    defaultValue: 1000,
                    decimalsLimit: 2,
                    onValueChange: (value, name) => handleChange(value, name),
                  },
                }}
              />
            </Grid>
          </Grid>
          <Grid container px={2} pt={3} direction="column">
            <Grid item>
              <Typography type="nunitoMedium18">Стоимость обучения</Typography>
            </Grid>
            <Grid item pt={0.5}>
              <TextField
                sx={{ width: '100%' }}
                InputProps={{
                  inputComponent: CurrencyInput,
                  inputProps: {
                    name: 'price',
                    placeholder: 'Please enter a number',
                    prefix: '',
                    defaultValue: 1000,
                    decimalsLimit: 2,
                    onValueChange: (value, name) => handleChange(value, name),
                  },
                }}
              />
            </Grid>
          </Grid>
          <Grid container px={2} pt={3} direction="column">
            <Grid item>
              <Typography type="nunitoMedium18">Формат встречи</Typography>
            </Grid>
            <Grid item>
              <List sx={{ width: '100%' }}>
                <ListItem divider disablePadding>
                  <Grid container justifyContent="space-between" py={1}>
                    <Grid item>
                      <Typography type="interRegular14">On-line</Typography>
                    </Grid>
                    <Grid item>
                      <Switch />
                    </Grid>
                  </Grid>
                </ListItem>
                <ListItem divider disablePadding>
                  <Grid container justifyContent="space-between" py={1}>
                    <Grid item>
                      <Typography type="interRegular14">Off-line</Typography>
                    </Grid>
                    <Grid item>
                      <Switch />
                    </Grid>
                  </Grid>
                </ListItem>
              </List>
            </Grid>
          </Grid>
          <Grid container px={2} pt={3} direction="column">
            <Grid item>
              <Typography type="nunitoMedium18">Разговорные языки</Typography>
            </Grid>
            <Grid item pt={0.5}>
              <Select value={20} sx={{ width: '100%' }} onChange={handleChange}>
                <MenuItem value={10}>Английский</MenuItem>
                <MenuItem value={20}>Русский</MenuItem>
                <MenuItem value={30}>Украинский</MenuItem>
              </Select>
            </Grid>
          </Grid>
          <Grid container px={2} pt={3} direction="column">
            <Grid item>
              <TypographyLinked type="nunitoMedium18">
                <Linkedin />
                <TitleLinked>Ссылка на Linkedin</TitleLinked>
              </TypographyLinked>
            </Grid>
            <Grid item pt={1}>
              <TextField sx={{ width: '100%' }} placeholder="https://linkedin.com/" />
            </Grid>
          </Grid>
          <Grid container px={2} pt={4} direction="column">
            <Grid item>
              <PrimaryButton>Сохранить изменения</PrimaryButton>
            </Grid>
          </Grid>
        </Form>
      </Formik>
    </MainWrapper>
  );
};
