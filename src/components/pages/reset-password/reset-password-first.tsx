import { Grid } from '@mui/material';
import { OnEventType } from '@type/on-event.type';

import { ResetPassword } from '@component/form/reset-password/reset-password';
import { MainWrapper } from '@component/main-wrapper/main-wrapper';
import { Title } from '@page-style/login/login.style';

interface ResetPasswordProps {
  onSubmit: OnEventType<any>;
}

export const ResetPasswordFirst = ({ onSubmit }: ResetPasswordProps) => (
  <MainWrapper hasFooter={false}>
    <Grid container px={2} pt={2}>
      <Grid item xs={12} pt={0.5} justifyContent="center">
        <Title type="interMedium20">Восстановить пароль</Title>
      </Grid>
      <Grid item xs={12}>
        <ResetPassword onSubmit={onSubmit} />
      </Grid>
    </Grid>
  </MainWrapper>
);
