import { Grid } from '@mui/material';
import { OnEventType } from '@type/on-event.type';

import { PrimaryButton } from '@component/button/button';
import { MainWrapper } from '@component/main-wrapper/main-wrapper';
import { Typography } from '@component/typography/typography';
import { Title } from '@page-style/login/login.style';

interface RegistrationSuccessProps {
  email: string;
  onBack: OnEventType;
}

export const ResetPasswordSuccess = ({ email, onBack }: RegistrationSuccessProps) => (
  <MainWrapper>
    <Grid container px={1.75} pt={2}>
      <Grid item xs={12} pt={0.5} justifyContent="center">
        <Title type="interMedium20">Пароль отправлен </Title>
      </Grid>
      <Grid item xs={12} pt={4}>
        <Typography type="interRegular16">Мы отправили Вам письмо на email: {email}</Typography>
      </Grid>
      <Grid item xs={12} pt={4}>
        <Typography type="interRegular16">Пожалуйста, следуйте шагам, указанным в письме.</Typography>
      </Grid>
      <Grid item xs={12} pt={6} justifyContent="center">
        <PrimaryButton onClick={onBack}>Вернуться назад</PrimaryButton>
      </Grid>
    </Grid>
  </MainWrapper>
);
