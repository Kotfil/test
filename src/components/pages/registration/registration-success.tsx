import { Grid } from '@mui/material';

import NextLink from 'next/link';

import { PrimaryButton } from '@component/button/button';
import { MainWrapper } from '@component/main-wrapper/main-wrapper';
import { Typography } from '@component/typography/typography';
import { LostPassword, Title } from '@page-style/login/login.style';

interface RegistrationSuccessProps {
  email: string;
}

export const RegistrationSuccess = ({ email }: RegistrationSuccessProps) => (
  <MainWrapper>
    <Grid container px={1.75} pt={2}>
      <Grid item xs={12} pt={0.5} justifyContent="center">
        <Title type="interMedium20">Регистрация</Title>
      </Grid>
      <Grid item xs={12} pt={4}>
        <Typography type="interRegular16">Мы отправили Вам письмо на email: {email}</Typography>
      </Grid>
      <Grid item xs={12} pt={4}>
        <Typography type="interRegular16">
          Пожалуйста, проверьте вашу почту и перейдите по ссылке, которая указана в письме.
        </Typography>
      </Grid>
      <Grid item xs={12} pt={4} justifyContent="center">
        <LostPassword type="interRegular14">Отправить повторно</LostPassword>
      </Grid>
      <Grid item xs={12} pt={6} justifyContent="center">
        <NextLink href="/">
          <PrimaryButton>Продолжить</PrimaryButton>
        </NextLink>
      </Grid>
    </Grid>
  </MainWrapper>
);
