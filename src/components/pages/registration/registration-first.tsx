import { Grid } from '@mui/material';
import { OnEventType } from '@type/on-event.type';

import NextLink from 'next/link';

import { SecondaryButton } from '@component/button/button';
import { RegistrationForm } from '@component/form/registration-form/registration-form';
import { MainWrapper } from '@component/main-wrapper/main-wrapper';
import { SocialButtons } from '@component/social/socialIcons';
import { TextDivider } from '@component/text-divider/text-divider';
import { Title } from '@page-style/login/login.style';

interface RegistrationFormProps {
  onSubmit: OnEventType<any>;
}

export const RegistrationFirst = ({ onSubmit }: RegistrationFormProps) => (
  <MainWrapper hasFooter={false}>
    <Grid container px={2} pt={2}>
      <Grid item xs={12} pt={0.5} justifyContent="center">
        <Title type="interMedium20">Регистрация</Title>
      </Grid>
      <Grid item xs={12}>
        <RegistrationForm onSubmit={onSubmit} />
      </Grid>
      <Grid item xs={12} pt={2}>
        <TextDivider>или</TextDivider>
      </Grid>
      <SocialButtons />
      <Grid item xs={12} pt={4}>
        <TextDivider>Есть аккаунт?</TextDivider>
      </Grid>
      <Grid item xs={12} pt={2}>
        <NextLink href="/login">
          <SecondaryButton>Войти</SecondaryButton>
        </NextLink>
      </Grid>
    </Grid>
  </MainWrapper>
);
