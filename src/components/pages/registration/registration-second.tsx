import { Grid } from '@mui/material';
import { OnEventType } from '@type/on-event.type';

import { RegistrationSecondForm } from '@component/form/registration-second-form/registration-second-form';
import { MainWrapper } from '@component/main-wrapper/main-wrapper';
import { Title } from '@page-style/login/login.style';

interface RegistrationFormProps {
  onSubmit: OnEventType<any>;
}

export const RegistrationSecond = ({ onSubmit }: RegistrationFormProps) => (
  <MainWrapper hasFooter={false}>
    <Grid container px={2} pt={2}>
      <Grid item xs={12} pt={0.5} justifyContent="center">
        <Title type="interMedium20">Регистрация</Title>
      </Grid>
      <Grid item xs={12}>
        <RegistrationSecondForm onSubmit={onSubmit} />
      </Grid>
    </Grid>
  </MainWrapper>
);
