import { Grid } from '@mui/material';
import { isMobile } from 'react-device-detect';
import styled from 'styled-components';

import { IconButton } from '@component/button/button';
import { Typography } from '@component/typography/typography';
import { ColorEnum } from '@enum/color.enum';

interface WrapperInterface {
  isActive: boolean;
}

export const Wrapper = styled(Grid)`
  position: fixed;
  top: 0;
  right: 0;
  left: 0;
  z-index: 1;
  height: 60px;
  padding: 0 16px;
  background-color: ${ColorEnum.White};
  box-shadow: 0 1px 1px #00000026;
`;

export const HamburgerWrapper = styled(Grid)`
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
`;

export const HamburgerButton = styled(IconButton)`
  margin: 0 auto;
`;

export const Title = styled(Typography)`
  flex: 1;
  color: ${ColorEnum.PrimaryColor};
  text-align: center;
  text-transform: uppercase;
`;

export const MenuWrapper = styled.div<WrapperInterface>`
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  z-index: 1;
  width: ${isMobile ? '100%' : '320px'};
  padding-top: 40px;
  overflow-x: hidden;
  overflow-y: auto;
  background-color: ${ColorEnum.White};
  transform: translateX(${({ isActive }) => (isActive ? 0 : '-100%')});
  transition: transform ${1 / 2}s cubic-bezier(0.33, 1, 0.68, 1);
`;
