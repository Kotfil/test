import { createContext, FC, useState } from 'react';

import { noop } from '@util/noop.util';

const initialNavigationContext = {
  isOpen: false,
  setOpen: noop,
};

export const NavigationContext = createContext(initialNavigationContext);

export const NavigationProvider: FC = ({ children }) => {
  const [isOpen, setOpen] = useState(initialNavigationContext.isOpen);

  return <NavigationContext.Provider value={{ isOpen, setOpen }}>{children}</NavigationContext.Provider>;
};
