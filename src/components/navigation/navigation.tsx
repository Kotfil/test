import { Grid } from '@mui/material';
import { useCallback, useContext } from 'react';
import { isMobile } from 'react-device-detect';
import { useHotkeys } from 'react-hotkeys-hook';
import { useSwipeable } from 'react-swipeable';

import { Icon } from '@component/icon/icon';

import { NavigationList } from './navigation-list/navigation-list';
import { NavigationContext } from './navigation.context';
import { HamburgerButton, HamburgerWrapper, MenuWrapper, Title, Wrapper } from './navigation.styles';

export const Navigation = () => {
  const { isOpen, setOpen } = useContext(NavigationContext);
  const handleOpen = useCallback(() => setOpen(true), []);
  const handleClose = useCallback(() => setOpen(false), []);

  useHotkeys('escape', handleClose);
  const handlers = useSwipeable({
    preventDefaultTouchmoveEvent: true,
    trackTouch: true,
    trackMouse: true,
    onSwipedLeft: () => isMobile && setOpen(false),
  });

  return (
    <>
      <Wrapper>
        <HamburgerWrapper container>
          <Grid container item xs={1}>
            <HamburgerButton onClick={handleOpen}>
              <Icon type="Hamburger" />
            </HamburgerButton>
          </Grid>
          <Grid container item xs={10}>
            <Title type="syncopateRegular24">BestTera</Title>
          </Grid>
          <Grid item xs={1} />
        </HamburgerWrapper>
      </Wrapper>
      <MenuWrapper isActive={isOpen} {...(isMobile && handlers)}>
        <NavigationList onClick={handleClose} />
      </MenuWrapper>
    </>
  );
};
