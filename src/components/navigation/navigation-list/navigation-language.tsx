import { FormControl, Grid, MenuItem } from '@mui/material';

import { Icon } from '@component/icon/icon';

import { ExpandedIcon, SelectLanguage } from './navigation-list.styles';

const values = [
  { value: 'ua', label: 'Українська' },
  { value: 'en', label: 'English' },
  { value: 'ru', label: 'Русский' },
];

const MenuProps = {
  MenuListProps: {
    style: {
      width: 250,
    },
  },
};

const IconComponent = () => <ExpandedIcon type="Expand" />;

export const NavigationLanguage = () => (
  <Grid container pt={2} pl={1}>
    <Grid item pt={1.25}>
      <Icon type="Language" />
    </Grid>
    <Grid item xs={9} pl={1}>
      <FormControl variant="standard" sx={{ m: 1, width: 300 }}>
        <SelectLanguage MenuProps={MenuProps} value={values[0].value} IconComponent={IconComponent}>
          {values.map(({ label, value }) => (
            <MenuItem key={value} value={value}>
              {label}
            </MenuItem>
          ))}
        </SelectLanguage>
      </FormControl>
    </Grid>
  </Grid>
);
