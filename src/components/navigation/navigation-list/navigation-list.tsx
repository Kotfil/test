import { Box, Divider, Grid, List } from '@mui/material';
import { OnEventType } from '@type/on-event.type';

import NextLink from 'next/link';

import { PrimaryButton, SecondaryButton } from '@component/button/button';
import { SocialIcons } from '@component/social/socialIcons';

import { NavigationLanguage } from './navigation-language';
import { NavigationListItem } from './navigation-list-item';
import { NavigationListOptions } from './navigation-list.options';
import { NavigationFooter, Wrapper } from './navigation-list.styles';

interface NavigationListProps {
  onClick: OnEventType;
}

export const NavigationList = ({ onClick }: NavigationListProps) => (
  <Wrapper mt={-3} px={1}>
    <Box pb={1}>
      <List>
        {NavigationListOptions.map(item => (
          <NavigationListItem key={item.title} item={item} onClick={onClick} />
        ))}
      </List>
    </Box>
    <Divider />
    <NavigationLanguage />
    <NavigationFooter px={1}>
      <Grid container direction="row">
        <Grid item xs={3} />
        <Grid item xs={6}>
          <SocialIcons />
        </Grid>
      </Grid>
      <Box pt={2}>
        <NextLink href="/login">
          <PrimaryButton>Войти</PrimaryButton>
        </NextLink>
      </Box>
      <Box pt={2}>
        <NextLink href="/registration">
          <SecondaryButton>Регистрация</SecondaryButton>
        </NextLink>
      </Box>
    </NavigationFooter>
  </Wrapper>
);
