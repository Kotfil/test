import { Box, ListItem, ListItemButton, ListItemIcon, Select } from '@mui/material';
import styled from 'styled-components';

import { Icon } from '@component/icon/icon';
import { ColorEnum } from '@enum/color.enum';

export const Wrapper = styled(Box)`
  display: flex;
  flex-direction: column;
  height: 100%;
`;

export const ItemIcon = styled(ListItemIcon)`
  width: 24px;
  min-width: 24px !important;
`;

export const NavListItem = styled(ListItem)`
  & > div:hover {
    background-color: ${ColorEnum.LightGrey} !important;
  }
`;

export const Item = styled(ListItemButton)`
  padding: 8px 0 8px 8px !important;
  border-radius: 8px !important;
`;

export const SelectLanguage = styled(Select)`
  /* stylelint-disable no-descending-specificity */
  width: 50%;

  ::before,
  & > div {
    border: none !important;
  }
`;

export const ExpandedIcon = styled(Icon)`
  margin-top: -4px;
`;

export const NavigationFooter = styled(Box)`
  margin-top: auto;
`;
