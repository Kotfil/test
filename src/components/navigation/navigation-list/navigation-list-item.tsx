import { Grid, ListItemText } from '@mui/material';
import { OnEventType } from '@type/on-event.type';

import { Icon } from '@component/icon/icon';
import { Link } from '@component/link/link';

import { Item, ItemIcon, NavListItem } from './navigation-list.styles';

interface NavigationListItemProps {
  item: any;
  onClick: OnEventType;
}

export const NavigationListItem = ({ item: { title, icon, link }, onClick }: NavigationListItemProps) => (
  <Link href={link ?? '#'}>
    <NavListItem disablePadding onClick={onClick}>
      <Item>
        <ItemIcon>
          <Icon type={icon} />
        </ItemIcon>
        <Grid item pl={2}>
          <ListItemText primary={title} />
        </Grid>
      </Item>
    </NavListItem>
  </Link>
);
