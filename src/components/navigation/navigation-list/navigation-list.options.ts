export const NavigationListOptions = [
  {
    title: 'Главная',
    icon: 'Home',
    link: '/',
  },
  {
    title: 'Профессионалы',
    icon: 'Professional',
  },
  {
    title: 'Справка',
    icon: 'Info',
    link: '/reference',
  },
  {
    title: 'Условия использования',
    icon: 'Uses',
    link: '/terms-of-use',
  },
  {
    title: 'Политика конфиденциальности',
    icon: 'Confident',
    link: '/confidentiality',
  },
  {
    title: 'Блог',
    icon: 'Blog',
    link: '/blog',
  },
];
