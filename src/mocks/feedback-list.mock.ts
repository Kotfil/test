export const mockFeedbackList = [
  {
    name: 'Cindy Kelley',
    avatar: 'https://randomuser.me/api/portraits/women/13.jpg',
    star: 4.5,
    feedback: 'Алина - отличный профессионал. Встреча прошла успешно. Всем рекомендую только Алину.',
    date: 1641447574000,
  },
  {
    name: 'Guy Hicks',
    avatar: 'https://randomuser.me/api/portraits/men/2.jpg',
    star: 3,
    feedback:
      'Алина очень помогла мне на консультации разобраться в веб дизайне. Еще не раз буду обращаться к ней за помощью. Очень благодарна ей. Она - настоящий профессионал своего дела.',
    date: 1641544774000,
  },
  {
    name: 'Lena Newman',
    avatar: 'https://randomuser.me/api/portraits/women/48.jpg',
    star: 5,
    feedback:
      'Алина очень помогла мне на консультации разобраться в веб дизайне. Еще не раз буду обращаться к ней за помощью. Очень благодарна ей. Она - настоящий профессионал своего дела.',
    date: 1641678334000,
  },
].map((item, index) => ({ ...item, id: `m${index}` }));
