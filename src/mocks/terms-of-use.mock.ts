export const mockTermsOfUseHeader = [
  {
    greeting:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae massa scelerisque, tincidunt magna vel, maximus risus. Pellentesque venenatis sodales mi vitae finibus. Cras semper sem tristique mauris mollis, non egestas justo scelerisque. Nunc finibus eget ligula eget ornare. Nam eget ligula ac purus volutpat pellentesque. Morbi sit amet eros eget felis congue facilisis.',

    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae massa scelerisque, tincidunt magna vel, maximus risus.',
  },
];
export const mockTermsOfUse = [
  {
    title: 'Сбор персональных данных',
    text:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque suscipit eros elit. Phasellus at elementum libero, sodales faucibus sapien. Curabitur in cursus dui. Suspendisse varius ex risus, sed pretium ipsum pellentesque nec. Sed viverra sem a ipsum malesuada, nec sodales nibh consequat. Cras imperdiet pellentesque nunc eu convallis. Sed ac tristique diam, a luctus lorem. Nullam feugiat sit amet justo in varius. Fusce risus dolor, lacinia sit amet placerat eu, imperdiet et nibh. Praesent fringilla elit et porttitor consequat. Praesent finibus dolor semper accumsan faucibus. Cras et euismod metus. Morbi scelerisque turpis at felis volutpat congue. Etiam accumsan ex a accumsan viverra.\n' +
      'Nullam at imperdiet orci. Donec tincidunt euismod porta. Mauris non consequat sapien. Suspendisse fringilla vehicula erat sit amet commodo. Morbi ullamcorper ac erat auctor gravida. Sed volutpat feugiat ex sit amet sollicitudin. Nunc facilisis, urna et suscipit scelerisque, quam risus aliquam tortor, et cursus leo enim id justo',
  },
  {
    title: 'Гарантия возврата средств',
    text: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque suscipit eros elit. Phasellus at elementum libero, sodales faucibus sapien. Curabitur in cursus dui. Suspendisse varius ex risus, sed pretium ipsum pellentesque nec. Sed viverra sem a ipsum malesuada, nec sodales nibh consequat. Cras imperdiet pellentesque nunc eu convallis. Sed ac tristique diam, a luctus lorem. Nullam feugiat sit amet justo in varius. Fusce risus dolor, lacinia sit amet placerat eu, imperdiet et nibh. Praesent fringilla elit et porttitor consequat. Praesent finibus dolor semper accumsan faucibus. Cras et euismod metus \n . Morbi scelerisque turpis at felis volutpat congue.   Etiam accumsan ex a accumsan viverra. 
       Nullam at imperdiet orci. Donec tincidunt euismod porta. Mauris non consequat sapien. Suspendisse fringilla vehicula erat sit amet commodo. Morbi ullamcorper ac erat auctor gravida. Sed volutpat feugiat ex sit amet sollicitudin. Nunc facilisis, urna et suscipit scelerisque, quam risus aliquam tortor, et cursus leo enim id justo`,
  },
];
