export const mockMentorsList = [
  {
    name: 'Cindy Kelley',
    avatar: 'https://randomuser.me/api/portraits/women/13.jpg',
    location: '2006 Samaritan Dr',
    price: 20,
    vote: 50,
    star: 4.5,
    experience: 4.5,
    category: 'Финансист',
    link: 'https://www.linkedin.com/in/ihaiduk/',
    skills: [
      {
        id: 'fin',
        name: 'Финансист',
      },
    ],
    tags: [
      {
        id: 'fin',
        name: 'Финансист',
      },
    ],
    language: [
      {
        id: 'ua',
        name: 'ua',
      },
      {
        id: 'en',
        name: 'en',
      },
    ],
    format: [
      {
        id: 'online',
        name: 'Online',
      },
      {
        id: 'offline',
        name: 'Offline',
      },
    ],
    services: [
      {
        id: 'Консультирование',
        name: 'Консультирование',
      },
      {
        id: 'Обучение',
        name: 'Обучение',
      },
      {
        id: 'Деловые знакомства',
        name: 'Деловые знакомства',
      },
    ],
    description:
      'Я профессионально занимаюсь веб дизайном уже несколько лет. В частности, дизайном UI/UX уже 4 года. Графикой и 3D дизайном - 3 года.',
  },
  {
    name: 'Guy Hicks',
    avatar: 'https://randomuser.me/api/portraits/men/2.jpg',
    location: '9979 First Street',
    price: 30,
    vote: 10,
    star: 3,
    experience: 3,
    category: 'Юрист',
    link: 'https://www.linkedin.com/in/ihaiduk/',
    skills: [
      {
        id: 'fin',
        name: 'Финансист',
      },
    ],
    tags: [
      {
        id: 'fin',
        name: 'Финансист',
      },
      {
        id: 'fin2',
        name: 'Юрист',
      },
      {
        id: 'fin3',
        name: 'Figma',
      },
      {
        id: 'fin4',
        name: 'Дизайн UI/UX',
      },
      {
        id: 'fin5',
        name: 'Графика',
      },
    ],
    language: [
      {
        id: 'ua',
        name: 'ua',
      },
      {
        id: 'en',
        name: 'en',
      },
    ],
    format: [
      {
        id: 'online',
        name: 'Online',
      },
    ],
    services: [
      {
        id: 'Консультирование',
        name: 'Консультирование',
      },
      {
        id: 'Обучение',
        name: 'Обучение',
      },
      {
        id: 'Деловые знакомства',
        name: 'Деловые знакомства',
      },
    ],
    description:
      'Я профессионально занимаюсь веб дизайном уже несколько лет. В частности, дизайном UI/UX уже 4 года. Графикой и 3D дизайном - 3 года.',
  },
  {
    name: 'Lena Newman',
    avatar: 'https://randomuser.me/api/portraits/women/48.jpg',
    location: '4692 Harrison Ct',
    price: 10,
    vote: 50,
    star: 5,
    experience: 5,
    category: 'Финансист',
    link: 'https://www.linkedin.com/in/ihaiduk/',
    skills: [
      {
        id: 'fin',
        name: 'Финансист',
      },
    ],
    tags: [
      {
        id: 'fin',
        name: 'Финансист',
      },
    ],
    language: [
      {
        id: 'ua',
        name: 'ua',
      },
      {
        id: 'en',
        name: 'en',
      },
    ],
    format: [
      {
        id: 'offline',
        name: 'Offline',
      },
    ],
    services: [
      {
        id: 'Консультирование',
        name: 'Консультирование',
      },
      {
        id: 'Обучение',
        name: 'Обучение',
      },
      {
        id: 'Деловые знакомства',
        name: 'Деловые знакомства',
      },
    ],
    description:
      'Я профессионально занимаюсь веб дизайном уже несколько лет. В частности, дизайном UI/UX уже 4 года. Графикой и 3D дизайном - 3 года.',
  },
].map((item, index) => ({ ...item, id: `m${index}` }));
