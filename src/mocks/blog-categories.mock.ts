export const mockBlogCategories = [
  {
    text: 'Советы экспертов',
    link: 'adviсe',
    count: '3',
  },
  {
    text: 'В мире технологий',
    link: 'technologies',
    count: '14',
  },
  {
    text: 'Профессионалы учат',
    link: 'professionals',
    count: '19',
  },
  {
    text: 'Деловые новости',
    link: 'news',
    count: '7',
  },
  {
    text: 'Консалтинг и общество',
    link: 'consulting',
    count: '11',
  },
];
