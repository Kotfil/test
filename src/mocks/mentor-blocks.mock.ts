export enum MentorButtonEnum {
  About = 'about',
  Feedback = 'feedback',
}

export const mentorBlocksMock = [
  {
    type: MentorButtonEnum.About,
    label: 'О профессионале',
  },
  {
    type: MentorButtonEnum.Feedback,
    label: 'Отзывы',
  },
];
