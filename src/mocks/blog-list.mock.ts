export const mockBlogList = [
  {
    image:
      'https://randompicturegenerator.com/img/people-generator/g5ed164e2a8dd9efa94ec8fd1fac2c66c699d08d3efa92a462fee77a08685f704a71cc12b7e8f45fc18dc499fc05a8b46_640.jpg',
    title: 'Как вам проводить консультации в офисе. Советы экспертов в 2022г.',
    subtitle: 'Советы экспертов',
    date: 1643447564000,
    readingDate: 1653447564000,
    firstName: 'Марина',
    lastName: 'Скляр',
    text:
      '   Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae massa scelerisque, tincidunt magna vel, maximus risus. Pellentesque venenatis sodales mi vitae finibus. Cras semper sem tristique mauris mollis, non egestas justo scelerisque. Nunc finibus eget ligula eget ornare. Nam eget ligula ac purus volutpat pellentesque. Morbi sit amet eros eget felis congue facilisis. Mauris euismod a neque ut placerat. In quis imperdiet massa. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec arcu sapien, finibus id porttitor a, maximus vel purus. Donec vulputate dapibus enim, vel luctus mi ultricies at. Proin pharetra accumsan justo, sit amet egestas magna tincidunt et. Fusce at augue enim. Praesent eu metus dolor.\n' +
      '\n' +
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque suscipit eros elit. Phasellus at elementum libero, sodales faucibus sapien. Curabitur in cursus dui. Suspendisse varius ex risus, sed pretium ipsum pellentesque nec. Sed viverra sem a ipsum malesuada, nec sodales nibh consequat. Cras imperdiet pellentesque nunc eu convallis. Sed ac tristique diam, a luctus lorem. Nullam feugiat sit amet justo in varius. Fusce risus dolor, lacinia sit amet placerat eu, imperdiet et nibh. Praesent fringilla elit et porttitor consequat. Praesent finibus dolor semper accumsan faucibus. Cras et euismod metus. Morbi scelerisque turpis at felis volutpat congue. Etiam accumsan ex a accumsan viverra.\n' +
      '\n' +
      'Nullam at imperdiet orci. Donec tincidunt euismod porta. Mauris non consequat sapien. Suspendisse fringilla vehicula erat sit amet commodo. Morbi ullamcorper ac erat auctor gravida. Sed volutpat feugiat ex sit amet sollicitudin. Nunc facilisis, urna et suscipit scelerisque, quam risus aliquam tortor, et cursus leo enim id justo. Nulla in faucibus nibh, cursus luctus metus. In hac habitasse platea dictumst. Mauris semper turpis at arcu porta iaculis. Vestibulum vel arcu ex. Duis sed nulla a sem mattis pharetra. Mauris varius quam ut venenatis volutpat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc velit elit, dignissim non augue vel, accumsan viverra mauris.\n',
  },
  {
    image:
      'https://randompicturegenerator.com/img/people-generator/g5ed164e2a8dd9efa94ec8fd1fac2c66c699d08d3efa92a462fee77a08685f704a71cc12b7e8f45fc18dc499fc05a8b46_640.jpg',
    title: 'Как вам проводить консультации в офисе. Советы экспертов в 2022г.',
    subtitle: 'Советы экспертов',
    date: 1645447574000,
    readingDate: 1653447564000,
    firstName: 'Вадим',
    lastName: 'Скляренко',
    text:
      '   Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae massa scelerisque, tincidunt magna vel, maximus risus. Pellentesque venenatis sodales mi vitae finibus. Cras semper sem tristique mauris mollis, non egestas justo scelerisque. Nunc finibus eget ligula eget ornare. Nam eget ligula ac purus volutpat pellentesque. Morbi sit amet eros eget felis congue facilisis. Mauris euismod a neque ut placerat. In quis imperdiet massa. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec arcu sapien, finibus id porttitor a, maximus vel purus. Donec vulputate dapibus enim, vel luctus mi ultricies at. Proin pharetra accumsan justo, sit amet egestas magna tincidunt et. Fusce at augue enim. Praesent eu metus dolor.\n' +
      '\n' +
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque suscipit eros elit. Phasellus at elementum libero, sodales faucibus sapien. Curabitur in cursus dui. Suspendisse varius ex risus, sed pretium ipsum pellentesque nec. Sed viverra sem a ipsum malesuada, nec sodales nibh consequat. Cras imperdiet pellentesque nunc eu convallis. Sed ac tristique diam, a luctus lorem. Nullam feugiat sit amet justo in varius. Fusce risus dolor, lacinia sit amet placerat eu, imperdiet et nibh. Praesent fringilla elit et porttitor consequat. Praesent finibus dolor semper accumsan faucibus. Cras et euismod metus. Morbi scelerisque turpis at felis volutpat congue. Etiam accumsan ex a accumsan viverra.\n' +
      '\n' +
      'Nullam at imperdiet orci. Donec tincidunt euismod porta. Mauris non consequat sapien. Suspendisse fringilla vehicula erat sit amet commodo. Morbi ullamcorper ac erat auctor gravida. Sed volutpat feugiat ex sit amet sollicitudin. Nunc facilisis, urna et suscipit scelerisque, quam risus aliquam tortor, et cursus leo enim id justo. Nulla in faucibus nibh, cursus luctus metus. In hac habitasse platea dictumst. Mauris semper turpis at arcu porta iaculis. Vestibulum vel arcu ex. Duis sed nulla a sem mattis pharetra. Mauris varius quam ut venenatis volutpat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc velit elit, dignissim non augue vel, accumsan viverra mauris.\n',
  },
  {
    image:
      'https://randompicturegenerator.com/img/people-generator/gfc47389984697c096f32cff6ef7f6a08ca7c646ac3d1766aeaa987c4404267bb8fb6877a32229a86121ff842e0fd7553_640.jpg',
    title: 'Встречаем Новый год в кругу техэкспертов. Что  интересного в мире AR/VR',
    subtitle: 'В мире технологий',
    date: 1647447574000,
    readingDate: 1653447564000,
    firstName: 'Олег',
    lastName: 'Глазов',
    text:
      'Cras semper sem tristique mauris mollis, non egestas justo scelerisque. Nunc finibus eget ligula eget ornare. Nam eget ligula ac purus volutpat pellentesque. Morbi sit amet eros eget felis congue facilisis. Mauris euismod a neque ut placerat. In quis imperdiet massa. Class aptent taciti' +
      ' sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec arcu sapien, finibus id porttitor a, maximus vel purus. Donec vulputate dapibus enim, vel luctus mi ultricies at. Proin pharetra accumsan justo, sit amet egestas magna tincidunt et. Fusce at augue enim. Praesent eu metus dolor.',
  },
  {
    image:
      'https://randompicturegenerator.com/img/people-generator/g07df17bde6cb8c935061806696d510dd733c1411024cf3aef775f71e7cfb8df5ba2ef99f85dddb80b7233f5424b5d8bd_640.jpg',
    title: 'Хотите стать профессиональным консультантом? Пробуем свои силы на платформах',
    subtitle: 'Профессионалы учат',
    date: 1641947574000,
    readingDate: 1653447564000,
    firstName: 'Марина',
    lastName: 'Скляр',
    text:
      'Nullam at imperdiet orci. Donec tincidunt euismod porta. Mauris non consequat sapien. Suspendisse fringilla vehicula erat sit amet commodo. Morbi ullamcorper ac erat auctor gravida. Sed volutpat feugiat ex sit amet sollicitudin. Nunc facilisis, urna ' +
      'et suscipit scelerisque, quam risus aliquam tortor, et cursus leo enim id justo. Nulla in faucibus nibh, cursus luctus metus. In hac habitasse platea dictumst.',
  },
].map((item, index) => ({ ...item, id: `b${index}` }));
