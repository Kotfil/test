export const mockConfidentialityHeader = [
  {
    title: 'Общие положения',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae massa scelerisque, tincidunt magna vel, maximus risus. Pellentesque venenatis sodales mi vitae finibus. Cras semper sem tristique mauris mollis, non egestas justo scelerisque. Nunc finibus eget ligula eget ornare. Nam eget ligula ac purus volutpat pellentesque. Morbi sit amet eros eget felis congue facilisis. Mauris euismod a neque ut placerat. In quis imperdiet massa. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec arcu sapien, finibus id porttitor a, maximus vel purus. Donec vulputate dapibus enim, vel luctus mi ultricies at. Proin pharetra accumsan justo, sit amet egestas magna tincidunt et. Fusce at augue enim. Praesent eu metus dolor.',
  },
];
export const mockConfidentialityBody = [
  {
    title: 'Как BESTTERA обрабатывает ваши персональные данные',
    text:
      'Nunc gravida ornare augue, et elementum odio condimentum in. In vulputate sem eget elit lacinia varius. Suspendisse consequat dapibus sodales. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer viverra egestas erat quis maximus. In pretium bibendum lacus in eleifend. Cras eleifend eget urna sed varius. Donec at libero dignissim, laoreet risus tempor, condimentum quam. Duis sagittis quam consectetur volutpat consectetur. Sed at pulvinar dui. Fusce id lobortis ex, quis rutrum magna. Praesent nisi erat, ullamcorper in sodales in, consequat vitae velit. Vivamus ac sem molestie, vehicula massa quis, vulputate lacus. Quisque euismod lorem sagittis neque accumsan fringilla. Nullam ac quam vitae sem ultrices rhoncus. Fusce cursus quam nec lectus varius commodo.\n' +
      '\n' +
      'Cras vitae metus pharetra, dignissim massa quis, vehicula purus. Etiam neque justo, posuere nec imperdiet in, posuere nec eros. Proin cursus venenatis sem. Aenean bibendum nec ipsum sed suscipit. Morbi lectus tortor, imperdiet non neque id, facilisis volutpat justo. Nunc non nisl felis. Etiam vestibulum bibendum tellus id aliquet. Aenean pretium nisi et rutrum porttitor. In ultrices ex id quam lacinia, quis scelerisque velit mollis. In varius semper neque, in ultrices libero facilisis pharetra.',
  },
  {
    title: 'Процессинг платежных систем',
    text: 'Nunc gravida ornare augue, et elementum odio condimentum in. In vulputate sem eget elit lacinia varius. Suspendisse consequat dapibus sodales. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer viverra egestas erat quis maximus. In pretium bibendum lacus in eleifend. Cras eleifend eget urna sed varius. Donec at libero dignissim, laoreet risus tempor, condimentum quam. Duis sagittis quam consectetur volutpat consectetur. Sed at pulvinar dui. Fusce id lobortis ex, quis rutrum magna. Praesent nisi erat, ullamcorper in sodales in, consequat vitae velit. Vivamus ac sem molestie, vehicula massa quis, vulputate lacus. Quisque euismod lorem sagittis neque accumsan fringilla. Nullam ac quam vitae sem ultrices rhoncus. Fusce cursus quam nec lectus varius commodo.',
  },
];
