export const mockRefoundPolicy = [
  {
    title: 'Данные платежных систем',
    description:
      'Lorem Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae massa scelerisque, tincidunt magna vel, maximus risus. Pellentesque venenatis sodales mi vitae finibus. Cras semper sem tristique mauris mollis, non egestas justo scelerisque. Nunc finibus eget ligula eget ornare. Nam eget ligula ac purus volutpat pellentesque. Morbi sit amet eros eget felis congue facilisis. Lorem Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae massa scelerisque, tincidunt magna vel, maximus risus. Pellentesque venenatis sodales mi vitae finibus. Cras semper sem tristique mauris mollis, non egestas justo scelerisque. Nunc finibus eget ligula eget ornare. Nam eget ligula ac purus volutpat pellentesque. Morbi sit amet eros eget felis congue facilisis.',
  },
  {
    title: 'Возврат средств',
    description:
      'Lorem Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae massa scelerisque, tincidunt magna vel, maximus risus. Pellentesque venenatis sodales mi vitae finibus. Cras semper sem tristique mauris mollis, non egestas justo scelerisque. Nunc finibus eget ligula eget ornare. Nam eget ligula ac purus volutpat pellentesque. Morbi sit amet eros eget felis congue facilisis. Lorem Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae massa scelerisque, tincidunt magna vel, maximus risus. Pellentesque venenatis sodales mi vitae finibus. Cras semper sem tristique mauris mollis, non egestas justo scelerisque. Nunc finibus eget ligula eget ornare. Nam eget ligula ac purus volutpat pellentesque. Morbi sit amet eros eget felis congue facilisis.',
  },
  {
    title: 'Транзакции',
    description:
      'Lorem Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae massa scelerisque, tincidunt magna vel, maximus risus. Pellentesque venenatis sodales mi vitae finibus. Cras semper sem tristique mauris mollis, non egestas justo scelerisque. Nunc finibus eget ligula eget ornare. Nam eget ligula ac purus volutpat pellentesque. Morbi sit amet eros eget felis congue facilisis. Lorem Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae massa scelerisque, tincidunt magna vel, maximus risus. Pellentesque venenatis sodales mi vitae finibus. Cras semper sem tristique mauris mollis, non egestas justo scelerisque. Nunc finibus eget ligula eget ornare. Nam eget ligula ac purus volutpat pellentesque. Morbi sit amet eros eget felis congue facilisis.',
  },
];
