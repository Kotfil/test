export const getQueryParam = (value?: string | string[]): string => {
  const [result] = Array.isArray(value) ? value : [value ?? ''];

  return result;
};
